//
//  Receipts_Data.h
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/31/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Receipts_Data : NSObject

@property (strong,nonatomic) NSString *vendor_name;
@property (strong,nonatomic) NSString *tax;
@property (strong,nonatomic) NSString *total;
@property (strong,nonatomic) NSString *receipt_image;
@property (strong,nonatomic) NSString *receipt_number;
@property (strong,nonatomic) NSString *tax_percent;
@property (strong,nonatomic) NSString *receipt_date;
@property (strong,nonatomic) NSString *selected_category;
@property (strong,nonatomic) NSString *receipt_description;
@property (strong,nonatomic) NSString *sales_tax;





@end
