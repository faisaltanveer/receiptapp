//
//  ViewController.m
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/22/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//
#import "ViewController.h"
#import "GPUImage.h"
#import "BJImageCropper.h"
#import "CropViewController.h"
#import "NSString+Levenshtein.h"
#import "AppDelegate.h"
#import "Receipts_Data.h"
#import "GeneratePDF.h"
#import "EditReceiptViewController.h"
#import "InfoViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize binarizedImage,cropImageFrame,imageProcessor,tableDataSourceArray,name,tax,total,recId,imageString,recDate,ReceiptDescription,Selected_Category,Sales_Tsx_Percentage,infoViewobj,sizeofimage,totalReceipts,pictureTaken;

UIImagePickerController *imagePicker;
UITapGestureRecognizer *tap;
NSMutableArray *years;

NSMutableArray *filteredYears;
NSMutableArray *allReceipts;

NSString *selectedYear = @"";
int indexSelected;

BOOL isGenerateReportTapped = NO;
BOOL isManualReceipt = NO;

NSString *selectedReportYear = @"";
int reportYearIndexSelected;

UITapGestureRecognizer *gestureRecognizer;


BOOL isPickerVisible = NO;

int YearPickerRow=0;
BOOL select_categoryTapped_Bool = NO;

- (void)viewDidLoad
{
     self.segmentControll.selectedSegmentIndex = -1;
     
     [[NSUserDefaults standardUserDefaults] setObject:[[NSNumber alloc]initWithBool:YES] forKey:ShouldFetchRecordsKey];
     
     [[NSUserDefaults standardUserDefaults] synchronize];
     
     if([[NSUserDefaults standardUserDefaults] objectForKey:@"scrolled"])
     {
          [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"scrolled"];
     }
     
     [super viewDidLoad];
     
//     arrayOfPicSizes = [[NSMutableArray alloc]init]; //waqar
     
     select_categoryTapped_Bool = NO;
     
    // NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
     //NSString *documentsDirectory = [paths objectAtIndex:0];
     //NSError * error;
     //NSArray * directoryContents =  [[NSFileManager defaultManager]
                                    // contentsOfDirectoryAtPath:documentsDirectory error:&error];
     //NSLog(@"directoryContents ====== %@",directoryContents);
     
     UIFont *customFont = [UIFont fontWithName:@"Cooper-Heavy" size:20.5];
     [_zonninsLabel setFont:customFont];
     [_zonninsLabel setTextColor:[UIColor blackColor]];
     [_zonninsLabel setHidden:YES];
     
     dataArray = [[NSMutableArray alloc]init];
     CategoryArrayForGenerateReport=[[NSMutableArray alloc]init];
     
     Selected_Category_Picker=[[NSMutableString alloc]initWithString:@"All Categories"];
     
     [CategoryArrayForGenerateReport addObject:@"All Categories"];
     [CategoryArrayForGenerateReport addObject:@"Business"];
     [CategoryArrayForGenerateReport addObject:@"Personal"];
     [CategoryArrayForGenerateReport addObject:@"Other"];
     
     [dataArray addObject:@"All Categories"];
     [dataArray addObject:@"Business"];
     [dataArray addObject:@"Personal"];
     [dataArray addObject:@"Other"];
 
     
     //    _zonninsLabel.layer.shadowColor = [[UIColor whiteColor] CGColor];
     //    _zonninsLabel.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
     //    _zonninsLabel.layer.shadowOpacity = 1.0f;
     //    _zonninsLabel.layer.shadowRadius = 1.5f;
     //Get Current Year into i2
     
     NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
     [formatter setDateFormat:@"yyyy"];
     int i2  = [[formatter stringFromDate:[NSDate date]] intValue];
     
//     [self.segmentControll setText:[NSString stringWithFormat:@"Sales Tax [Select Year]"]];
     
     [self.segmentControll setTitle:@"Select Year" forSegmentAtIndex:0];
     //Create Years Array from 1960 to This year
     years = [[NSMutableArray alloc] init];
     [years addObject:[NSString stringWithFormat:@"All Receipts"]];
     for (int i=i2; i>=1960; i--) {
          [years addObject:[NSString stringWithFormat:@"%d",i]];
     }
     
     [_yearPickerView setHidden:YES];
     
     [self.categoryPickerview setHidden:YES];
     
     [_pickerTopBar setHidden:YES];
     
     _searchBar.delegate = self;
     
     
     [self.navigationController setNavigationBarHidden:YES];
     [self clearCacheData];
     
     tap.delegate = self;
     
     tap= [[UITapGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(dismissKeyboard)];
     
     
     UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
     gesture.delegate = self;
     // if labelView is not set userInteractionEnabled, you must do so
//    [_yearLabel setUserInteractionEnabled:YES];
//     [_yearLabel  addGestureRecognizer:gesture];
     
     
     
     UITapGestureRecognizer* gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(totaltapped:)];
     gesture1.delegate = self;
     // if labelView is not set userInteractionEnabled, you must do so
     [_totalSpentLabel setUserInteractionEnabled:YES];
     [_totalSpentLabel addGestureRecognizer:gesture1];
     
     
     UITapGestureRecognizer* category_gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(select_categoryTapped)];
     category_gesture.delegate = self;
     // if labelView is not set userInteractionEnabled, you must do so
//     [self.selectedcategorylabel setUserInteractionEnabled:YES];
//     [self.selectedcategorylabel addGestureRecognizer:category_gesture];

     gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
     [self.tableView addGestureRecognizer:gestureRecognizer];


}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
     return YES;
}

//awais

-(void)select_categoryTapped
{
//     self.segmentControll.selectedSegmentIndex = -1;
     select_categoryTapped_Bool = YES;
     
     self.YearORCategorySelection.text=@"Select Category";
     
     [_yearPickerView setHidden:YES];
     
     //NSLog(@"category Label Tapped");
     if (isPickerVisible == NO)
     {
          isPickerVisible = YES;
          [self.categoryPickerview setHidden:NO];
          [_pickerTopBar setHidden:NO];
     }
     else
     {
          isPickerVisible = NO;
          [self.categoryPickerview setHidden:YES];
          
          //[_yearPickerView setHidden:YES]; // waqar1
          
          [_pickerTopBar setHidden:YES];        
          
     }
//     self.segmentControll.selectedSegmentIndex = -1;
}



-(void)totaltapped:(id)sender
{

     NSString *total1 = [NSString stringWithFormat:@"%f",[[NSUserDefaults standardUserDefaults]floatForKey:@"totalSalesTax"]] ;
     
     double myfloat = [total1 floatValue];
     if(myfloat>9999999999)
     {
          total1 = [NSString stringWithFormat:@"$ %0.2f",[[NSUserDefaults standardUserDefaults]floatForKey:@"totalSalesTax"]] ;
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Total amount " message:total1 delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:Nil, nil];
          [alert show];
     }
     else
     {
          //do nothing
     }
     

}






//awais
-(void)labelTapped:(id)sender
{
     
     //NSLog(@"Year Label Tapped");
     
     //[_yearLabel setText:[NSString stringWithFormat:@"Sales Tax  All Receipts"]];
     
     [_yearPickerView reloadAllComponents];
     
     self.YearORCategorySelection.text=@"Select Year";  //
     [self.categoryPickerview setHidden:YES];
     
     
     if (isPickerVisible == NO)
     {
          
          isPickerVisible = YES;
          [_yearPickerView setHidden:NO];
          [_pickerTopBar setHidden:NO];
          
     }
     else
     {
          isPickerVisible = NO;
          [_yearPickerView setHidden:YES];
          [_pickerTopBar setHidden:YES];
     }
    // self.segmentControll.selectedSegmentIndex = -1;
}


-(void)dismissKeyboard
{
     gestureRecognizer.cancelsTouchesInView = NO;
     [_searchBar resignFirstResponder];
}


-(void)viewDidDisappear:(BOOL)animated
{
     [super viewDidDisappear:YES];
}

- (void)didReceiveMemoryWarning
{
     [super didReceiveMemoryWarning];
}

-(void) viewDidAppear:(BOOL)animated
{
     [self.searchBar resignFirstResponder];
     self.searchBar.text=@"";
     
     if (self.tableView.contentSize.height > self.tableView.frame.size.height)
     {
          CGPoint offset = CGPointMake(0, self.tableView.contentSize.height -     self.tableView.frame.size.height);
          [self.tableView setContentOffset:offset animated:YES];
     }

}

-(void) takePicPressed:(id)sender
{
     isManualReceipt = NO;
     [self actionLaunchAppCamera];
     
     //    CGSize size;
     //
     //    size.width=640;e
     //    size.height=960;
     //    UIImage *newImage = [UIImage imageNamed:@"sample50.jpg"];
     //    UIImage *image  = [imageProcessor resizeImage:newImage toSize:size];
     //
     //    binarizedImage = [imageProcessor applyBrightness:image];
     //    binarizedImage = [imageProcessor fixRGB:image];
     //    [self performSegueWithIdentifier:@"cropViewController" sender:nil];
     
     // Above code loads the image without camera
     
}

-(void)viewWillAppear:(BOOL)animated
{
     [self dismissKeyboard];
     self.searchBar.text=@"";
     self.segmentControll.selectedSegmentIndex = -1;
     [self.view removeGestureRecognizer:tap];
     //self.searchBar.text=@"";
     _appDel =[[AppDelegate alloc] init];
     
//     self.selectedcategorylabel.text =@"[Select Category]:";
     [self.segmentControll setTitle:@"Select Category" forSegmentAtIndex:1];
     
     
     imageProcessor = [[ImageProcessing alloc] init];
     
     
     if ([[[NSUserDefaults standardUserDefaults] objectForKey:ShouldFetchRecordsKey] boolValue]) {
          
          
          
          [[NSUserDefaults standardUserDefaults] setObject:[[NSNumber alloc]initWithBool:NO] forKey:ShouldFetchRecordsKey];
          
          [[NSUserDefaults standardUserDefaults] synchronize];
     }
     
     allReceipts = [_appDel retrieveAllReciepts];
     
     /*
      
     if (indexForUpdation == -1)
     {
           allReceipts = [_appDel retrieveAllReciepts];
     }
     else
     {
          [allReceipts replaceObjectAtIndex:indexForUpdation withObject:[_appDel getReceiptWithId:recId]];
          
          indexForUpdation = -1;
     }
     */
    
     
     [_yearPickerView setHidden:YES];  // // waqar1
     [_pickerTopBar setHidden:YES];
     
     if(YearPickerRow != 0){  //waqar1
          selectedYear = [years objectAtIndex:0];
          [self.segmentControll setTitle:@"All Receipts" forSegmentAtIndex:0];
//          [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax All Receipts"]];
     }
     
     if (![selectedYear caseInsensitiveCompare:@"All Receipts"] || ![selectedYear caseInsensitiveCompare:@""])
     {
          [self.segmentControll setTitle:@"Select Year" forSegmentAtIndex:0];
//          [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax [Select Year]"]];
          _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[[NSUserDefaults standardUserDefaults] floatForKey:@"totalSalesTax"]];
          tableDataSourceArray = [[NSMutableArray alloc]initWithArray:allReceipts];
          [self.tableView reloadData];
     }
     else
     {
          [self.segmentControll setTitle:[NSString stringWithFormat:@"%@",[years objectAtIndex:indexSelected] ] forSegmentAtIndex:0];
//          [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax           %@",[years objectAtIndex:indexSelected]]];
          _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[self filterReceiptsOnYear:[[years objectAtIndex:indexSelected] integerValue]]];
     }
     
//     [_yearPickerView reloadAllComponents];
//     [_yearPickerView selectRow:0 inComponent:0 animated:YES];
//     
//     [self.categoryPickerview reloadAllComponents];
//     [self.categoryPickerview selectRow:0 inComponent:0 animated:YES];
     
     

}


/////// Camera Opens and Closes


-(void)actionLaunchAppCamera
{
     
     if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
     {
          
          imagePicker = [[UIImagePickerController alloc]init];
          imagePicker.delegate = self;
          imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
          imagePicker.allowsEditing = NO;
          imagePicker.showsCameraControls = YES;
          imagePicker.navigationBarHidden = YES;
          imagePicker.toolbarHidden = YES;
          [self presentViewController:imagePicker animated:YES completion:nil];
          
     }else{
          UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Camera Unavailable"
                                                         message:@"Unable to find a camera on your device."
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
          [alert show];
          alert = nil;
     }
     
}




-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo{
     
     [self dismissViewControllerAnimated:YES completion:nil];
     
     CGSize size;
     
     size.width=640;
     size.height=960;
     NSData *jpegImage = UIImageJPEGRepresentation(image,0.0);
     UIImage *newImage = [[UIImage alloc] initWithData:jpegImage];
     
     image  = [imageProcessor resizeImage:newImage toSize:size];
     newImage = nil;
     
     binarizedImage = [imageProcessor applyBrightness:image];
     binarizedImage = [imageProcessor fixRGB:image];
     
     //waqar
     
     NSData *imageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation((binarizedImage), 0.5)];
     int imageSize = imageData.length;
     //NSLog(@"SIZE OF IMAGE: %i ", imageSize);
     sizeofimage = imageSize;
     
//     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//     arrayOfPicSizes = [[defaults objectForKey:@"arrayOfPicSizes"] mutableCopy];
//     
//     if(!arrayOfPicSizes)
//     {
//          arrayOfPicSizes = [[NSMutableArray alloc]init];
//     }
//     
//     NSNumber *number = [NSNumber numberWithInt:imageSize];
//     [arrayOfPicSizes addObject:number];
//     [defaults setObject:arrayOfPicSizes forKey:@"arrayOfPicSizes"];
//     [defaults synchronize];
     
     [self performSegueWithIdentifier:@"cropViewController" sender:nil];
     
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     
  
     if([allReceipts count]==0)
     {
          NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
          [defaults setObject:nil forKey:@"arrayOfPicSizes"];
     }
     
     
     if ([segue.identifier isEqualToString:@"cropViewController"]) {
          CropViewController *destViewController = segue.destinationViewController;
          destViewController.imageToCrop  = binarizedImage;
          destViewController.isManual = isManualReceipt;
          binarizedImage = nil;
          imageProcessor = nil;
          
          if(sizeofimage != 0.0)
          {
               destViewController.imageSizeInBytes = sizeofimage;
          }
          else
          {
               destViewController.imageSizeInBytes = 0.0;
          }
          
     }
     if ([segue.identifier isEqualToString:@"editReceipt"]) {
          
          EditReceiptViewController *destViewController = segue.destinationViewController;
          
          destViewController.editVendor = name;
          destViewController.editTax = tax;
          destViewController.editTotal = total;
          destViewController.editId = recId;
          destViewController.editImage = imageString;
          destViewController.editDate = recDate;
          destViewController.Receipt_Description=ReceiptDescription;
          destViewController.Selected_Category=Selected_Category;
          destViewController.Sales_Tax_Percantage=Sales_Tsx_Percentage;
          
          //waqar
               [_yearPickerView setHidden:YES];
               [self.categoryPickerview setHidden:YES];
               [_pickerTopBar setHidden:YES];
     }
     
     if ([segue.identifier isEqualToString:@"infoController"]) {
          InfoViewController *destViewController = segue.destinationViewController;
          destViewController.totalReceipts = totalReceipts ;
          destViewController.delegate = self;
          
          //waqar
          
               [_yearPickerView setHidden:YES];
               [self.categoryPickerview setHidden:YES];
               [_pickerTopBar setHidden:YES];
        
          //waqar
//          if(sizeofimage != 0.0)
//          {
//               destViewController.imageSizeInBytes = sizeofimage;
//          }
//          else
//          {
//               destViewController.imageSizeInBytes = 0.0;
//          }
          
     }
}


// Table view delegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     totalReceipts = [tableDataSourceArray count];
     
    // [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"scrolled"
      
     if([[NSUserDefaults standardUserDefaults] objectForKey:@"scrolled"])
     {
          if([tableDataSourceArray count] > 6)
          {
               [self.tableView setContentOffset:CGPointMake (0, (tableDataSourceArray.count-6) * 31)];
          }
     }
     
     return [tableDataSourceArray count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     //NSLog(@"Row tapped %d",[indexPath row]);
     Receipts_Data *rec = [tableDataSourceArray objectAtIndex:indexPath.row];
     //NSLog(@"%@",[rec receipt_date]);
     name = [rec vendor_name];
     tax = [rec tax];
     total = [rec total];
     
     recId= [rec receipt_number];
     imageString = [rec receipt_image];
     recDate = [rec receipt_date];
     
     Sales_Tsx_Percentage= [rec tax_percent];
     
     Selected_Category=[rec selected_category];
     ReceiptDescription=[rec receipt_description];
     
     
     [self performSegueWithIdentifier:@"editReceipt" sender:nil];
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     
     static NSString *CellIdentifier = @"Cell1";
     
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     
     UILabel *vendLabel/*1001*/,*spentLabel/*1002*/,*taxLabel/*1003*/,*datelabel/*1004*/;
     
     if (cell == nil)
     {
          cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
          
          vendLabel = [[UILabel alloc] initWithFrame:CGRectMake(150, 10, 80, cell.frame.size.height)]; //
          vendLabel.tag = 1001;
          
          spentLabel = [[UILabel alloc] initWithFrame:CGRectMake(250, 10, 63, cell.frame.size.height)];
          spentLabel.tag = 1002;
          
          taxLabel =  [[UILabel alloc] initWithFrame:CGRectMake(80, 10, 60, cell.frame.size.height)];
          taxLabel.tag = 1003;
          
          datelabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 66, cell.frame.size.height)];
          datelabel.tag = 1004;
          
          
          
          spentLabel.textAlignment =UITextAlignmentLeft;
          datelabel.textAlignment =NSTextAlignmentLeft;
          taxLabel.textAlignment =UITextAlignmentLeft;
          
          
          vendLabel.font =  [UIFont fontWithName:@"ArialRoundedMTBold" size:11.0];
          spentLabel.font = vendLabel.font;
          taxLabel.font = vendLabel.font;
          datelabel.font = vendLabel.font;
          
          
          vendLabel.textColor =  [UIColor grayColor];
          spentLabel.textColor = [UIColor grayColor];
          taxLabel.textColor =   [UIColor grayColor];
          datelabel.textColor =  [UIColor grayColor];
          
          [vendLabel setBackgroundColor: [UIColor clearColor]];
          [taxLabel setBackgroundColor:  [UIColor clearColor]];
          [spentLabel setBackgroundColor:[UIColor clearColor]];
          [datelabel setBackgroundColor: [UIColor clearColor]];
          
          cell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
          cell.selectionStyle = UITableViewCellSelectionStyleDefault;
          
          [cell  addSubview:vendLabel];
          [cell  addSubview:spentLabel];
          [cell  addSubview:taxLabel];
          [cell  addSubview:datelabel];
          [cell setUserInteractionEnabled:YES];
          cell.backgroundColor = [UIColor clearColor];
     }
     
     else
     {
          vendLabel = (UILabel *) [cell viewWithTag:1001];
          spentLabel = (UILabel *) [cell viewWithTag:1002];
          taxLabel = (UILabel *) [cell viewWithTag:1003];
          datelabel = (UILabel *) [cell viewWithTag:1004];
     }
     
     NSLog(@"Cell for row All receipt count %li",[allReceipts count]);
     
     Receipts_Data *data = (Receipts_Data *)[allReceipts objectAtIndex:indexPath.row];
     
     NSLog(@"%@",[data total]);
     
     vendLabel.text =[[tableDataSourceArray objectAtIndex:indexPath.row] vendor_name];
     
     if([[[tableDataSourceArray objectAtIndex:indexPath.row]total] isEqualToString:@""])
     {
          spentLabel.text =[ NSString stringWithFormat:@"$0.00%@",[[tableDataSourceArray objectAtIndex:indexPath.row] total]];
     }
     else
     {
          NSString *total1 = [NSString stringWithFormat:@"%@",[[tableDataSourceArray objectAtIndex:indexPath.row] total]];
          float total2 = [total1 floatValue];
          
          spentLabel.text =[ NSString stringWithFormat:@"$%0.2f",total2];
          
     }

     
     taxLabel.text =[ NSString stringWithFormat:@"$%@",[[tableDataSourceArray objectAtIndex:indexPath.row] tax]];
     
     datelabel.text =[[tableDataSourceArray objectAtIndex:indexPath.row] receipt_date];
     
     return cell;
     
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
     return 1;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
     if (editingStyle == UITableViewCellEditingStyleDelete) {
          // Delete the row from the data source
          Receipts_Data *reciept = [tableDataSourceArray objectAtIndex:indexPath.row];
          
          float totalAmt = [[NSUserDefaults standardUserDefaults] floatForKey:@"totalSalesTax"];
          float receiptTax =[[reciept tax] floatValue];
          
          totalAmt = totalAmt - receiptTax;
          
          [[NSUserDefaults standardUserDefaults] setFloat:totalAmt forKey:@"totalSalesTax"];
          [[NSUserDefaults standardUserDefaults] synchronize];
          _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[[NSUserDefaults standardUserDefaults] floatForKey:@"totalSalesTax"]];
          
          [_appDel deleteReceiptWithId:[reciept receipt_number]];
          
          ///////////////////////////   Faisal Changes ///////////////////////////
          //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
          //////////////////////   Faisal Changes /////////////////////////
          UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Receipt Deleted"] message:@"Receipt Has Been Deleted Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
          
          ////waqar
          NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//          float fileSize = [defaults floatForKey:@"fileSize"];
//          
//          fileSize = fileSize-41943;
//          [defaults setFloat:fileSize forKey:@"fileSize"];
          
          int index = indexPath.row;
          //NSLog(@"Index Path %d",index);
          
          NSMutableArray *array = [[NSMutableArray alloc] init];
          array = [[defaults objectForKey:@"arrayOfPicSizes"]mutableCopy];
          
          [array removeObjectAtIndex:index];
//          [array insertObject:nil atIndex:index];          
          
          [defaults setObject:array forKey:@"arrayOfPicSizes"];
          [defaults synchronize];
          
          
          alertView.tag = 2;
          alertView.alertViewStyle = UIAlertViewStyleDefault;
          [alertView show];
          
          allReceipts = [_appDel retrieveAllReciepts];
          
          //[_tableView reloadData];
          NSLog(@"array count == %d",tableDataSourceArray.count);
          
          tableDataSourceArray = [[NSMutableArray alloc]initWithArray:allReceipts];
          NSLog(@"array count == %d",tableDataSourceArray.count);
          [NSTimer scheduledTimerWithTimeInterval: 0.1 target: self selector: @selector(tableReload) userInfo: nil repeats: NO];
         
          
          // Alteration Required Here : Inam
          
          if (![selectedYear caseInsensitiveCompare:@"All Receipts"] || ![selectedYear caseInsensitiveCompare:@""])
          {
//               [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax [Select Year]"]]; //[Select Year]
               [self.segmentControll setTitle:@"Select Year" forSegmentAtIndex:0];
               _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[[NSUserDefaults standardUserDefaults] floatForKey:@"totalSalesTax"]];
               //tableDataSourceArray = [NSMutableArray new];
               //tableDataSourceArray = [_appDel retrieveAllReciepts];
          }
          else
          {
               [self.segmentControll setTitle:[NSString stringWithFormat:@"%@", [years objectAtIndex:indexSelected]] forSegmentAtIndex:0];
//               [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax           %@",[years objectAtIndex:indexSelected]]];
               _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[self filterReceiptsOnYear:[[years objectAtIndex:indexSelected] integerValue]]];
          }
     }
     else if (editingStyle == UITableViewCellEditingStyleInsert) {
          // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
}

-(void)tableReload
{
     [_tableView reloadData];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
     
     
     //NSLog(@"%d",section);
     UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];
     [headerView setBackgroundColor:[UIColor colorWithRed:146.0/255 green:173.0/255 blue:207.0/255 alpha:1.0]];
     
     
     UILabel *vendorLabel = [[UILabel alloc] initWithFrame:CGRectMake(150, 0, 103, 22)];
     
     [vendorLabel setFont:[UIFont fontWithName:@"Arial Rounded MT Bold" size:15]];
     [vendorLabel setTextColor:[UIColor whiteColor]];
     [vendorLabel setText:@"Vendor"];
     [vendorLabel setBackgroundColor:[UIColor clearColor]];
     vendorLabel.textAlignment=NSTextAlignmentLeft;
     
     UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 51, 22)];
     
     [dateLabel setFont:[UIFont fontWithName:@"Arial Rounded MT Bold" size:15]];
     [dateLabel setTextColor:[UIColor whiteColor]];
     [dateLabel setText:@"Date"];
     [dateLabel setBackgroundColor:[UIColor clearColor]];
     dateLabel.textAlignment=NSTextAlignmentLeft;
     
     
     
     UILabel *totalLabel = [[UILabel alloc] initWithFrame:CGRectMake(250, 0, 73, 22)];
     
     [totalLabel setFont:[UIFont fontWithName:@"Arial Rounded MT Bold" size:15]];
     [totalLabel setTextColor:[UIColor whiteColor]];
     [totalLabel setText:@"Total"];
     [totalLabel setBackgroundColor:[UIColor clearColor]];
     totalLabel.textAlignment=NSTextAlignmentLeft;
     
     UILabel *taxLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, 60, 22)];
     
     [taxLabel setFont:[UIFont fontWithName:@"Arial Rounded MT Bold" size:15]];
     [taxLabel setTextColor:[UIColor whiteColor]];
     [taxLabel setText:@"Tax"];
     [taxLabel setBackgroundColor:[UIColor clearColor]];
     taxLabel.textAlignment=NSTextAlignmentLeft;
     
     [headerView addSubview:vendorLabel];
     [headerView addSubview:totalLabel];
     [headerView addSubview:taxLabel];
     [headerView addSubview:dateLabel];
     
     return headerView;
}


- (UIImage *)stringToUIImage:(NSString *)string
{
     NSData *data = [[NSData alloc]initWithBase64EncodedString:string
                                                       options:NSDataBase64DecodingIgnoreUnknownCharacters];
     
     return [UIImage imageWithData:data];
}

- (IBAction)generateReport:(id)sender {
     
     isGenerateReportTapped = YES;
     
     
     if (isPickerVisible == NO) {
          
          [_yearPickerView reloadAllComponents];
          
          isPickerVisible = YES;
          [_yearPickerView setHidden:NO];
          [_pickerTopBar setHidden:NO];
          
     }else{          
          isPickerVisible = NO;
          //    isGenerateReportTapped = NO;
          //    [_yearPickerView setHidden:YES];  // // waqar1
          //    [_pickerTopBar setHidden:YES];
     }
     
     /*
      if ([tableDataSourceArray count] == 0) {
      
      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Alert"] message:@"No Receipts Found To Generate Report" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
      alertView.tag = 2;
      alertView.alertViewStyle = UIAlertViewStyleDefault;
      [alertView show];
      }
      else{
      NSString *fileName = [NSString stringWithFormat:@"Receipt_Report.pdf"];
      NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
      NSString *documentsDirectory = [paths objectAtIndex:0];
      NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
      GeneratePDF *pdf = [[GeneratePDF alloc] init];
      NSString* filePath =[pdf generatePdfWithFilePath:pdfFileName:tableDataSourceArray];
      [self displayComposerSheet:filePath:fileName];
      }*/
}



-(void)displayComposerSheet:(NSString*)file : (NSString*)fileName
{
     
     NSData *pdfData = [NSData dataWithContentsOfFile:file];
     
     
     MFMailComposeViewController * mailVC = [[MFMailComposeViewController alloc] init];
     //[mailVC setToRecipients:@[@"qahorizon2@gmail.com"] ];
     [mailVC setSubject:[NSString stringWithFormat:@"Zonnins Sales Tax Scanner PDF"]];
     [mailVC addAttachmentData:pdfData mimeType:@"application/pdf" fileName:fileName];
     [mailVC setMailComposeDelegate:self];
     //[mailVC setMessageBody:@"This is sample email for Zonnis Sales Tax Scanner" isHTML:NO];
     //[mailVC setMessageBody:@"" isHTML:NO];
     if ([MFMailComposeViewController canSendMail])
     {
          // Create and show composer
          [self presentViewController:mailVC animated:YES completion:nil];
     }
     else
     {
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                          message:@"Your device doesn't support the composer sheet"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
          [alert show];
     }
     
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
     [searchBar resignFirstResponder];
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
     // Notifies users about errors associated with the interface
     switch (result)
     {
          case MFMailComposeResultCancelled:
               NSLog(@"Result: canceled");
               break;
          case MFMailComposeResultSaved:
               NSLog(@"Result: saved");
               break;
          case MFMailComposeResultSent:
               NSLog(@"Result: sent");
               break;
          case MFMailComposeResultFailed:
               NSLog(@"Result: failed");
               break;
          default:
               NSLog(@"Result: not sent");
               break;
     }
     
     [self dismissModalViewControllerAnimated:YES];
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
     
  
     
          if (![searchBar.text compare:@""]) {
          if (![selectedYear caseInsensitiveCompare:@"All Receipts"] || ![selectedYear caseInsensitiveCompare:@""]) {
               [self.segmentControll setTitle:@"Select Year" forSegmentAtIndex:0];
//               [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax [Select Year]"]];
               _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[[NSUserDefaults standardUserDefaults] floatForKey:@"totalSalesTax"]];
               //tableDataSourceArray = [NSMutableArray new];
               //tableDataSourceArray = [_appDel retrieveAllReciepts];
               
               //tableDataSourceArray = allReceipts;
               tableDataSourceArray = [[NSMutableArray alloc]initWithArray:allReceipts];
               
               [self.tableView reloadData];
          }else{
               [self.segmentControll setTitle:[NSString stringWithFormat:@"%@", [years objectAtIndex:indexSelected]] forSegmentAtIndex:0];
//               [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax %@",[years objectAtIndex:indexSelected]]];
               _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[self filterReceiptsOnYear:[[years objectAtIndex:indexSelected] integerValue]]];
               
          }
          
     }
     
     else{
          
          if (![selectedYear caseInsensitiveCompare:@"All Receipts"] || ![selectedYear caseInsensitiveCompare:@""]) {
//               [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax [Select Year]"]];
               [self.segmentControll setTitle:@"Select Year" forSegmentAtIndex:0];
               _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[[NSUserDefaults standardUserDefaults] floatForKey:@"totalSalesTax"]];
               tableDataSourceArray = [[NSMutableArray alloc]init];
               tableDataSourceArray = [_appDel readFilteredVendors:searchText];
               [self.tableView reloadData];
          }else{
               
               [self.segmentControll setTitle:[NSString stringWithFormat:@"%@", [years objectAtIndex:indexSelected]] forSegmentAtIndex:0];
//               [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax           %@",[years objectAtIndex:indexSelected]]];
               _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[self filterReceiptsOnYear:[[years objectAtIndex:indexSelected] integerValue]]];
               tableDataSourceArray = [self readFilteredVendors:searchText];
               [self.tableView reloadData];
               
          }          
     }
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
     [searchBar resignFirstResponder];
     
}
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
//     [self.view addGestureRecognizer:tap];
     return YES;
     
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
     
  //   [self.view removeGestureRecognizer:tap];
     [searchBar resignFirstResponder];
     return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
      [self.searchBar resignFirstResponder];
}

-(void)clearCacheData{
     
     NSFileManager *fileManager = [NSFileManager defaultManager];
     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,   YES);
     NSString *documentsDirectory = [paths objectAtIndex:0];
     NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:
                           [NSString stringWithFormat:@"tessdata"]];
     
     NSError *error = nil;
     if(![fileManager removeItemAtPath: fullPath error:&error]) {
          NSLog(@"Tessdata not found:%@", error);
     } else {
          NSLog(@"TessData removed: %@", fullPath);
     }
     
     NSString *fullPathPDF = [documentsDirectory stringByAppendingPathComponent:
                              [NSString stringWithFormat:@"Receipt_Report.pdf"]];
     
     error = nil;
     if(![fileManager removeItemAtPath: fullPathPDF error:&error]) {
          NSLog(@"PDF not found:%@", error);
     } else {
          NSLog(@"PDF removed: %@", fullPath);
     }
     
}

- (IBAction)manualEnterPressed:(id)sender
{
     isManualReceipt = YES;
     [self actionLaunchAppCamera];
}



-(float)filterReceiptsOnYear:(int)year{

     
     // NSString *yearString = [NSString stringWithFormat:@"%d",year];
     NSString *yearString = [NSString stringWithFormat:@"%d",Selected_Year];
     
     filteredYears = [NSMutableArray new];
     Receipts_Data *receipt;
     float taxForYear = 0;
     
//     [segment titleForSegmentAtIndex:segment.selectedSegmentIndex];
     
     
     for (int i = 0; i< [allReceipts count]; i++)
     {
          receipt =  (Receipts_Data*)[allReceipts objectAtIndex:i];
          //if (([receipt.receipt_date rangeOfString:yearString].location != NSNotFound || Selected_Year==-1) && [receipt.selected_category isEqualToString:self.selectedcategorylabel.text]    )
          if (([receipt.receipt_date rangeOfString:yearString].location != NSNotFound || Selected_Year==-1) && ([receipt.selected_category isEqualToString:[self.segmentControll titleForSegmentAtIndex:1]] || [[self.segmentControll titleForSegmentAtIndex:1] isEqualToString:@"Select Category"]  ||   [[self.segmentControll titleForSegmentAtIndex:1] isEqualToString:@"All Categories"]  )    )
               
          {
               //NSLog(@"self.selectedcategorylabel.text :%@",self.selectedcategorylabel.text);
               //NSLog(@"receipt.selected_category :%@",receipt.selected_category);
               
               NSLog(@"Receipt For year found");
               taxForYear += [receipt.tax floatValue];
               [filteredYears addObject:receipt];
          }
     }
     tableDataSourceArray = filteredYears;
     [_tableView reloadData];
     return taxForYear;
     
}

#pragma mark PickerView


- (NSInteger)numberOfComponentsInPickerView: (UIPickerView*)thePickerView {
     
     
     if(thePickerView.tag==963)
     {
          if(isGenerateReportTapped)
          {
               return 2;
          }
          else
          {
               return 1;
          }
     }
     else
     {
          return 1;
     }
     
     return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
     if(thePickerView.tag==963)
     {
          if(component==0)
          {
               return [years count];
          }
          else
          {
               return [CategoryArrayForGenerateReport count];
          }
     }
     else
     {
          return [dataArray count];
     }
}




/////////////////////need to change//////////////////




- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
     if(thePickerView.tag==963)
     {
          if(component==0)
          {
               return [years objectAtIndex:row];
          }
          else
          {
               return [CategoryArrayForGenerateReport objectAtIndex:row];
          }
          
     }
     else
     {
          return   [dataArray objectAtIndex:row];
     }
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
     
     if(pickerView.tag==963)
     {
          if(component==0)
          {
               if(row!=0)
               {
                    Selected_Year= [[years objectAtIndex:row] intValue];
               }
               else
               {
                    Selected_Year=-1;
               }
               
               if (isGenerateReportTapped == YES)
               {
                    
                    reportYearIndexSelected = row;
                    selectedReportYear = [years objectAtIndex:row];
                    [_yearPickerView reloadAllComponents];
                    
               }
               else
               {
                    //NSLog(@"Year Selected is : %@",[years objectAtIndex:row]);
                    
                    selectedYear = [years objectAtIndex:row];
                    indexSelected = row;
                    
                    if (row != 0)
                    {
                         [self.segmentControll setTitle:[NSString stringWithFormat:@"%@", [years objectAtIndex:row]] forSegmentAtIndex:0];
//                         [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax           %@",[years objectAtIndex:row]]];
                         _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[self filterReceiptsOnYear:[[years objectAtIndex:row] integerValue]]];
                    }
                    else
                    {
                         selectedYear = @"All Receipts";  // waqar1
                         
                         [self.segmentControll setTitle:@"All receipts" forSegmentAtIndex:0];
//                         [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax All Receipts"]]; //[Select Year] waqar
                         //  _totalSpentLabel.text = [NSString stringWithFormat:@"$ %.2f",[[NSUserDefaults standardUserDefaults] floatForKey:@"totalSalesTax"]];
                         _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[self filterReceiptsOnYear:-1]];
                         
                         //     tableDataSourceArray = allReceipts ;
                         //     [_tableView reloadData];
                    }
               }
          }          
          else
          {
               Selected_Category_Picker=[CategoryArrayForGenerateReport objectAtIndex:row];
          }
     }
     else
     {
          [self.segmentControll setTitle:[NSString stringWithFormat:@"%@", [dataArray objectAtIndex:row]] forSegmentAtIndex:1];
//          [self.segmentControll :[dataArray objectAtIndex:row]];
          _totalSpentLabel.text = [NSString stringWithFormat:@"Sales Tax: $ %.2f",[self filterReceiptsOnYear:-1]];
     }
     
     if(row != 0){
          YearPickerRow = row;
     }
     else{
          YearPickerRow = 0;
     }
     
}



- (NSMutableArray *) readFilteredVendors:(NSString *)vendorName {
     
     NSMutableArray *receipts = [[NSMutableArray alloc] init];
     for (Receipts_Data *receipt in filteredYears) {
          if([self matchPattern:[receipt.vendor_name lowercaseString]  :[vendorName lowercaseString]]){
               //NSLog(@"%@, ", receipt.vendor_name);
               [receipts addObject:receipt];
          }
          
     }
     if ([receipts count]>0) {
          return receipts;
     }
     else
          return nil;
     
}

-(BOOL) matchPattern:(NSString*)sentence : (NSString *)word{
     
     
     if ([sentence rangeOfString:word].location != NSNotFound) {
          //NSLog(@"Yes it does contain that word");
          return YES;
     }
     
     return NO;
}




-(NSMutableArray*)filterReceiptsOnYearForReport:(int)year
{
     NSString *yearString = [NSString stringWithFormat:@"%d",year];
     filteredYears = [NSMutableArray new];
     Receipts_Data *receipt;
     float taxForYear = 0;
     
     for (int i = 0; i< [allReceipts count]; i++) {
          receipt =  (Receipts_Data*)[allReceipts objectAtIndex:i];
          if ([receipt.receipt_date rangeOfString:yearString].location != NSNotFound
              
              &&( [receipt.selected_category isEqualToString:Selected_Category_Picker] || [Selected_Category_Picker isEqualToString:@"All Categories"])
              ) {
               //NSLog(@"Receipt For year found");
               taxForYear += [receipt.tax floatValue];
               [filteredYears addObject:receipt];
          }
     }
     return filteredYears;
}

- (IBAction)doneButton:(id)sender {
     
     //waqar1
     [_yearPickerView setHidden:YES];
     [_pickerTopBar setHidden:YES];

     
     if(![self.YearORCategorySelection.text isEqualToString:@"Select Category"])
     {
          if (isGenerateReportTapped == YES)
          {
               [_yearPickerView reloadAllComponents];  //waqar1
               
               isGenerateReportTapped = NO;
               
               NSMutableArray *reportReceipts = [self filterReceiptsOnYearForReport:[[years objectAtIndex:reportYearIndexSelected] integerValue]];
               
               //NSLog(@"reportReceipts %@",reportReceipts);
               
               if ([reportReceipts count]>0)
               {
                    [self genReports:reportReceipts];
               }
               else
               {
                    
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Receipts Found"
                                                                   message:[NSString stringWithFormat:@"No receipts found for the selected year"]
                                                                  delegate:nil
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil, nil];
                    [alert show];
                    alert = nil;
               }
               
               [self labelTapped:sender];
          }
          else
          {
               
               [self labelTapped:sender];
          }
     }
     
     else
     {
          [self select_categoryTapped];
          if (isGenerateReportTapped == YES)
          {
               [_yearPickerView reloadAllComponents];
               isGenerateReportTapped = NO;
               
               NSMutableArray *reportReceipts = [self filterReceiptsOnYearForReport:[[years objectAtIndex:reportYearIndexSelected] integerValue]];
                              
               if ([reportReceipts count]>0)
               {
                    [self genReports:reportReceipts];
               }
               else
               {
                    
                    
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Receipts Found"
                                                                   message:[NSString stringWithFormat:@"No receipts found for the selected year"]
                                                                  delegate:nil
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil, nil];
                    [alert show];
                    alert = nil;
                    
               }               
               [self labelTapped:sender];
          }
          
     }
     
     //waqar
     if(YearPickerRow==0 && select_categoryTapped_Bool == NO){
          [self.segmentControll setTitle:@"All reciepts" forSegmentAtIndex:0];
//          [_yearLabel setText:[NSString stringWithFormat:@"Sales Tax All Receipts"]];
     }
     
     if(isPickerVisible == YES){
          [_yearPickerView setHidden:YES];
          [_pickerTopBar setHidden:YES];
     }
     
     self.segmentControll.selectedSegmentIndex = -1;
}

- (IBAction)infoButtonTapped:(id)sender {
     
//     [self pickerView:_yearPickerView didSelectRow:0 inComponent:0];
//     [self pickerView:_categoryPickerview didSelectRow:0 inComponent:0];
     
     //pictureTaken = FALSE;
     [self performSegueWithIdentifier:@"infoController" sender:nil];
}

-(void)genReports:(NSMutableArray*)receiptsForReportArray{
     
     
     NSString *fileName = [NSString stringWithFormat:@"Receipt_Report.pdf"];
     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
     NSString *documentsDirectory = [paths objectAtIndex:0];
     NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
     GeneratePDF *pdf = [[GeneratePDF alloc] init];
     NSString* filePath =[pdf generatePdfWithFilePath:pdfFileName:receiptsForReportArray];
     [self displayComposerSheet:filePath:fileName];    
     
     
}
-(void)setSizeofImageToZero
{
     sizeofimage = 0.0;
}

- (IBAction)SegmentControllAction:(id)sender {
     
     
//     
//     
//     isPickerVisible = YES;
//     [_yearPickerView setHidden:NO];
//     [_pickerTopBar setHidden:NO];
//     
     
     
//    self.segmentControll.selectedSegmentIndex = -1;
    
    if(self.segmentControll.selectedSegmentIndex==0)
    {
         self.segmentControll.selectedSegmentIndex = -1;
//         [self.categoryPickerview removeFromSuperview];
//         [self.view addSubview:self.yearPickerView];
         [self labelTapped:sender];
         
    }
    else
    {
         self.segmentControll.selectedSegmentIndex = -1;
//         [self.yearPickerView removeFromSuperview];
//         [self.view addSubview:self.categoryPickerview];
         [self select_categoryTapped];
    }
     
    
}
@end
