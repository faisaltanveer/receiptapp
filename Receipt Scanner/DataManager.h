//
//  DataManager.h
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/27/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

@property (strong,nonatomic) NSString *vendorName;
@property (strong,nonatomic) NSString *totalAmount;
@property (strong,nonatomic) UIImage  *image;
@property (strong,nonatomic) NSString *tax;

@end
