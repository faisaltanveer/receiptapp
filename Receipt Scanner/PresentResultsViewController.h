//
//  PresentResultsViewController.h
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/31/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import "ViewController.h"
#import "Receipts_Data.h"
#import "OALSimpleAudio.h"

#define ShouldFetchRecordsKey @"ShouldFetch"

@interface PresentResultsViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UIPickerViewDelegate>
{
    NSMutableArray *dataArray;
    UIPickerView *myPickerView ;
    NSMutableArray *arrayOfPicSizes;
}

@property (weak, nonatomic) IBOutlet UITextField *vendorNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *taxTextField;
@property (weak, nonatomic) IBOutlet UIImageView *receiptImage;
@property (weak, nonatomic) IBOutlet UITextField *totalTextField;
@property (strong, nonatomic) NSString *venName;
@property (strong, nonatomic) NSString *total;
@property (strong, nonatomic) NSString *tax;
@property (weak, nonatomic) IBOutlet UILabel *taxPercLbl;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *Category_String;
@property (strong, nonatomic) NSString *Address_String;
@property (weak, nonatomic) IBOutlet UITextField *salesTaxPercentageTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateTextField;
@property (strong, nonatomic) NSString *recieptImage;
@property (weak, nonatomic) IBOutlet UIView *pickerViewTopBar;
@property BOOL isManualRec;
- (IBAction)doneTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *SelectionLabel;

- (IBAction)saveReceiptPressed:(id)sender;
- (IBAction)backPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *Category;
@property (weak, nonatomic) IBOutlet UITextView *Description;
@property (weak, nonatomic) IBOutlet UILabel *Category_Label;
@property (weak, nonatomic) IBOutlet UILabel *Description_Label;
@property(nonatomic, retain)IBOutlet UIScrollView * Fields_Scroll;

@property (assign, nonatomic) int imageSizeInBytes;


@end
