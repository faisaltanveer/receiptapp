//
//  AppDelegate.m
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/22/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import "AppDelegate.h"
#import "Receipts_Data.h"

@implementation AppDelegate
@synthesize managedObjectContext,managedObjectModel,persistentStoreCoordinator,firstRun;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
//    NSString *string = @"12$$.9.666  3 .";
//    NSString *trimmedString = [string stringByTrimmingCharactersInSet:
//                               [NSCharacterSet characterSetWithCharactersInString:@".$123"]];
//    NSLog(@"%@", trimmedString);
//    
   
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (![defaults objectForKey:@"firstRun"]){
        //flag doesnt exist then this IS the first run
        self.firstRun = TRUE;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setFloat:49655980.0 forKey:@"fileSize"];
        
        
        //store the flag so it exists the next time the app starts
        [defaults setObject:[NSDate date] forKey:@"firstRun"];
        
        [defaults synchronize];
        
    }else{
        //flag does exist so this ISNT the first run
        self.firstRun = FALSE;
    }
    
   
//    if ( ! [[NSUserDefaults standardUserDefaults]boolForKey:@"firstRun"] ) {
//        
//        
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        [defaults setFloat:49655980.0 forKey:@"fileSize"];
//        
//        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstRun"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
    
    
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];

    //NSLog(@"Directory content \n %@",directoryContent);
    
    

    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



- (NSManagedObjectContext *) managedObjectContext {
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"Receipt_Scanner.sqlite"]];
    NSError *error = nil;
    
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                  initWithManagedObjectModel:[self managedObjectModel]];
    if(![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                 configuration:nil URL:storeUrl options:nil error:&error])
    {
        NSLog(@"error:::::%@",error);
    }
    
    return persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

-(void)insertResults:(Receipts_Data *)data
{
    // Grab the context
    NSManagedObjectContext *context = [self managedObjectContext];
    
    Receipts_Data *receipt = [NSEntityDescription insertNewObjectForEntityForName:@"Receipts_Data" inManagedObjectContext:context];
    
    [receipt setVendor_name:[data vendor_name]];
    [receipt setTax:[data tax]];
    [receipt setTotal:[data total]];
    [receipt setTax_percent:[data tax_percent]];
    receipt.receipt_image = data.receipt_image;
    [receipt setReceipt_number:[data receipt_number]];
    [receipt setReceipt_date:[data receipt_date]];
    [receipt setSelected_category:[data selected_category]];
    [receipt setReceipt_description:[data receipt_description]];
//    [receipt setSales_tax:[data sales_tax]];
    
    //Save everything
    NSError *error = nil;
    
    if ([context save:&error])
    {
        NSLog(@"The save was successful!");
    }
    else
    {
        NSLog(@"The save wasn't successful: %@", [error userInfo]);
    }
}

-(NSMutableArray *)retrieveAllReciepts
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Construct a fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Receipts_Data"
                                              inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    
    NSSortDescriptor *sorter = [NSSortDescriptor sortDescriptorWithKey:@"receipt_number" ascending:YES selector:@selector(localizedStandardCompare:)];
    
    [fetchRequest setSortDescriptors:@[sorter]];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    return  [[NSMutableArray alloc]initWithArray:fetchedObjects];
}

- (NSMutableArray *) readFilteredVendors:(NSString *)vendorName
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Construct a fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Receipts_Data"
                                              inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"vendor_name CONTAINS[cd] %@ ",vendorName];

    [fetchRequest setPredicate:predicate];
    
   NSSortDescriptor *sorter = [NSSortDescriptor sortDescriptorWithKey:@"receipt_number" ascending:YES selector:@selector(localizedStandardCompare:)];
    
    [fetchRequest setSortDescriptors:@[sorter]];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    return  [[NSMutableArray alloc]initWithArray:fetchedObjects];
}

/*
-(BOOL) matchPattern:(NSString*)sentence : (NSString *)word
{
        if ([sentence rangeOfString:word].location != NSNotFound) {
            //NSLog(@"Yes it does contain that word");
            return YES;
        }
    
    return NO;
}
*/

- (void) deleteReceiptWithId:(NSString *)recId
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Receipts_Data"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"receipt_number == %@ ", recId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    NSManagedObject *event = [fetchedObjects objectAtIndex:0];
    [context deleteObject:event];
    
    if ([context save:&error])
    {
        NSLog(@"The delete was successful!");
    }
    else
    {
        NSLog(@"The delete wasn't successful: %@", [error localizedDescription]);
    }
    
}




- (Receipts_Data*) getReceiptWithId:(NSString *)recId
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Receipts_Data"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"receipt_number == %@ ", recId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    if (error == nil && fetchedObjects != nil && [fetchedObjects count] > 0)
    {
        return (Receipts_Data*)[fetchedObjects objectAtIndex:0];
    }
    else
    {
        return nil;
        NSLog(@"The save wasn't successful: %@", [error localizedDescription]);
    }
}

/*
// Sorting Array.
-(NSMutableArray*) bubbleSortArray:(NSMutableArray*)unsortedArray
{
    while (TRUE)
    {
        BOOL hasSwapped = NO;
        
        for (int i=0; i<unsortedArray.count; i++)
        {
            /** out of bounds check */
            /*if (i < unsortedArray.count-1)
            {
                NSUInteger currentIndexValue = [[(Receipts_Data*)unsortedArray[i] receipt_number] integerValue];
                
                NSUInteger nextIndexValue    = [[(Receipts_Data*)unsortedArray[i+1] receipt_number] integerValue];
                
                if (currentIndexValue > nextIndexValue)
                {
                    hasSwapped = YES;
                    [self swapFirstIndex:i withSecondIndex:i+1 inMutableArray:unsortedArray];
                }
            }
            
        }
        
        if (!hasSwapped){
            break;
        }
    }
    
    return unsortedArray;
}


-(void)swapFirstIndex:(NSUInteger)firstIndex withSecondIndex:(NSUInteger)secondIndex inMutableArray:(NSMutableArray*)array{
    
    NSNumber* valueAtFirstIndex = array[firstIndex];
    NSNumber* valueAtSecondIndex = array[secondIndex];
    
    [array replaceObjectAtIndex:firstIndex withObject:valueAtSecondIndex];
    [array replaceObjectAtIndex:secondIndex withObject:valueAtFirstIndex];
}
*/

@end
