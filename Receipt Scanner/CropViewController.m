//
//  CropViewController.m
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/22/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)



#import "CropViewController.h"

#define SHOW_PREVIEW NO

@interface CropViewController ()
{
    MBProgressHUD *HUD;
}

@end

@implementation CropViewController
@synthesize imageToCrop,imageView,imageCropper,imageCropperForDetails,binarizedImage,imageProcessor,isManual;
UIImagePickerController *imagePicker;
UIAlertView *waitForOCR;
NSString *data;
float tax;
float total;
NSString *vendorName;
NSString *address;
BOOL firstPass = YES;
UIImage *vendorNameImage;
UIImage *totalAndTaxImage;
UIImage *addressImage;

int scanStep = 1;

BOOL isCancled = NO;

int imageSize;

int cropper = 1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    imageSize = _imageSizeInBytes;
    
    [self.navigationController setNavigationBarHidden:YES];
    
}
-(void)viewDidAppear:(BOOL)animated{

    if(self.isManual == NO){

    scanStep = 1;
    [_cropStepsLabel setText:@"Please Select The Receipt Area"];
    [_scanButton setImage:[UIImage imageNamed:@"next.png"] forState:UIControlStateNormal];
    
    
    }else{
    /*ali
    scanStep = 1;
    [_cropStepsLabel setText:@"Please Select The Receipt Area"];
    [_scanButton setImage:[UIImage imageNamed:@"prepare-result.png"] forState:UIControlStateNormal];
    */
        scanStep = 1;
        [_cropStepsLabel setText:@"Please Select The Receipt Area"];
        [_scanButton setImage:[UIImage imageNamed:@"next.png"] forState:UIControlStateNormal];
    
    }
}
- (void)viewWillAppear:(BOOL)animated{
    imageProcessor = [[ImageProcessing alloc] init];

    imageCropper = [[BJImageCropper alloc] initWithImage:imageToCrop andMaxSize:CGSizeMake(320, 365)];
    //NSLog(@"_cropImageView Frame: %f  %f",_cropImageView.frame.size.width,_cropImageView.frame.size.height);
    imageCropper.center = self.view.center;
    [self.view addSubview:imageCropper];
    imageCropper.imageView.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    imageCropper.imageView.layer.shadowRadius = 8.0f;
    imageCropper.cropView.layer.opacity=0.2f;
    imageCropper.cropView.layer.borderColor=[[UIColor blueColor] CGColor];
    [imageCropper addObserver:self forKeyPath:@"crop" options:NSKeyValueObservingOptionNew context:nil];
    
    _scanButton.hidden = NO;
    
    if (SHOW_PREVIEW) {
            imageView.image = [self.imageCropper getCroppedImage];
            imageView.clipsToBounds = YES;
            imageView.layer.borderColor = [[UIColor clearColor] CGColor];
        }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)updateDisplay {
    
	
    if (SHOW_PREVIEW) {
        imageView.image = [self.imageCropper getCroppedImage];
		imageView.backgroundColor=[UIColor clearColor];
		imageView.frame = CGRectMake(0,0,320, 460);

    }

}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([object isEqual:self.imageCropper] && [keyPath isEqualToString:@"crop"]) {
        [self updateDisplay];
    }
}

- (IBAction)scanVendorNamePressed:(id)sender {

    
    if (self.isManual == NO) {
        
    
    if(scanStep == 1){
        
        vendorNameImage = [imageCropper getCroppedImage];
        [_cropStepsLabel setText:@"Good! Now Select The Payement Details"];
        scanStep+=1;
    
    }else if(scanStep == 2){
     
        [_cropStepsLabel setText:@"Looks Good! Now Tap Prepare Results"];
        [_scanButton setImage:[UIImage imageNamed:@"prepare-result.png"] forState:UIControlStateNormal];
        scanStep += 1;
        totalAndTaxImage = [imageCropper getCroppedImage];
        
    }else if (scanStep == 3){
        
        scanStep = 1;
        
//        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
//        [self.navigationController.view addSubview:HUD];
//        
//        HUD.delegate = self;
//        HUD.labelText = @"Processing...";
//        HUD.detailsLabelText = @"Please wait";
//        HUD.square = YES;
//        
//       
//        [HUD showWhileExecuting:@selector(processOcrAt:) onTarget:self withObject:vendorNameImage animated:YES];
//        
        
       // [ActivityIndicatorBox showActivityIndicatorWithLabel:@"processing...." forView:self.view];
        
        //1 2 uncomment for scanning
        //waqar 1

  
        //waqar 2
        
        
        if (SYSTEM_VERSION_GREATER_THAN(@"7.1.2")) {
//            [self performSelector:@selector(processOcrAt:) withObject:vendorNameImage];
//                HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
//                    [self.navigationController.view addSubview:HUD];
//                    HUD.delegate = self;
//                    HUD.labelText = @"Processing...";
//                    HUD.detailsLabelText = @"Please wait";
//                    HUD.square = YES;
//            
//                    [HUD showWhileExecuting:@selector(processOcrAt:) onTarget:self withObject:vendorNameImage animated:YES];
//            
            _scanButton.hidden = YES;
            
            waitForOCR=[[UIAlertView alloc] initWithTitle:@"Processing..." message:@"Please wait." delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
            
            waitForOCR.delegate=self;
            [waitForOCR setTag:200];
            [waitForOCR show];
            
            
//            [self processOcrAt:vendorNameImage];
            [NSThread detachNewThreadSelector:@selector(processOcrAt:) toTarget:self withObject:vendorNameImage];
        }
        else
        {
            _scanButton.hidden = YES;
            
            waitForOCR=[[UIAlertView alloc] initWithTitle:@"Processing..." message:@"Please wait." delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
            
            
            
            waitForOCR.delegate=self;
            [waitForOCR setTag:200];
            [waitForOCR show];
            
            [NSThread detachNewThreadSelector:@selector(processOcrAt:) toTarget:self withObject:vendorNameImage];
        }
        

        
        //[self processOcrAt:vendorNameImage];
        
        
        //waqar, remove it for scanning
        //[self performSegueWithIdentifier:@"presentResults" sender:nil];
        
        isCancled = NO;
    }
        
    }else{
            if(scanStep == 1){
                
                vendorNameImage = [imageCropper getCroppedImage];
                [_cropStepsLabel setText:@"Good! Now Select The Payement Details"];
                scanStep+=1;
                
            }else if(scanStep == 2){
                
                [_cropStepsLabel setText:@"Looks Good! Now Tap Prepare Results"];
                [_scanButton setImage:[UIImage imageNamed:@"prepare-result.png"] forState:UIControlStateNormal];
                scanStep += 1;
                totalAndTaxImage = [imageCropper getCroppedImage];
                
            }else if (scanStep == 3){
                
                scanStep = 1;
                if (SYSTEM_VERSION_GREATER_THAN(@"7.1.2")) {
                    
                    _scanButton.hidden = YES;
                    
                    waitForOCR=[[UIAlertView alloc] initWithTitle:@"Processing..." message:@"Please wait." delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
                    
                    waitForOCR.delegate=self;
                    [waitForOCR setTag:200];
                    [waitForOCR show];
                    [NSThread detachNewThreadSelector:@selector(processOcrAt:) toTarget:self withObject:vendorNameImage];
                }
                else
                {
                    _scanButton.hidden = YES;
                    
                    waitForOCR=[[UIAlertView alloc] initWithTitle:@"Processing..." message:@"Please wait." delegate:self cancelButtonTitle:nil  otherButtonTitles:nil, nil];
                    
                    waitForOCR.delegate=self;
                    [waitForOCR setTag:200];
                    [waitForOCR show];
                    
                    [NSThread detachNewThreadSelector:@selector(processOcrAt:) toTarget:self withObject:vendorNameImage];
                }
                isCancled = NO;
            }
        /* ali[self performSegueWithIdentifier:@"presentResults" sender:nil];*/
    }
    
   // [NSThread detachNewThreadSelector:@selector(processOcrAt:) toTarget:self withObject:imageToCrop];
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 200) {
    
        isCancled = YES;
    
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"presentResults"]) {
        
        if (SYSTEM_VERSION_GREATER_THAN(@"7.1.2")) {
            [imageCropper removeObserver:self forKeyPath:@"crop"];
        }
        else{
            //[imageCropper removeObserver:self forKeyPath:@"crop"];
        }
        if (self.isManual == NO) {
            
            PresentResultsViewController *results = segue.destinationViewController;
            results.imageSizeInBytes = imageSize;
            results.tax = [[NSString alloc] initWithFormat:@"%.2f",tax];
            results.total = [[NSString alloc] initWithFormat:@"%.2f",total];
            
            //results.venName = vendorName;   //waqar1
            results.recieptImage = [self imageToNSString:self.imageToCrop];
            results.address = address;
            results.isManualRec = isManual;
//            PresentResultsViewController *results = segue.destinationViewController;
//            results.imageSizeInBytes = imageSize;
//            results.tax = @"";
//            results.total = @"";
//            results.venName = @"";
//            results.recieptImage = [self imageToNSString:imageCropper.getCroppedImage];
//            results.address = @"";
//            results.isManualRec = isManual;            
            
        }else{
            PresentResultsViewController *results = segue.destinationViewController;
            results.imageSizeInBytes = imageSize;
            results.total = [[NSString alloc] initWithFormat:@"%.2f",total];
            results.tax = @"";
            results.venName = @"";
            /*aliresults.recieptImage = [self imageToNSString:imageCropper.getCroppedImage];*/
            results.recieptImage = [self imageToNSString:self.imageToCrop];
            results.address = @"";
            results.isManualRec = isManual;
        }
    }
}


- (NSString *)imageToNSString:(UIImage *)image
{
    NSData *imageData = UIImagePNGRepresentation(image);
    
    
    return [self base64forData:imageData];
}

- (NSString*)base64forData:(NSData*)theData {
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] ;
}



-(void) processOcrAt: (UIImage *) image{
    

    CGSize size;
    
    size.width=image.size.width;
    size.height=image.size.height;
    
    NSData *jpegImage = UIImageJPEGRepresentation(image,1.0);
    UIImage *newImage = [[UIImage alloc] initWithData:jpegImage];
    
    image  = [imageProcessor resizeImage:newImage toSize:size];
    newImage = nil;
    image = [imageProcessor doBinarize:image];

    
    Tesseract *tesseract = [[Tesseract alloc] initWithDataPath:@"tessdata" language:@"eng"];
    
    
    [tesseract setVariableValue:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.$()#=-:,!" forKey:@"tessedit_char_whitelist"]; //limit search
    
    [tesseract setImage:image]; //image to check
    [tesseract recognize];
    
    if (SYSTEM_VERSION_GREATER_THAN(@"7.1.2")) {
        [self ocrProcessingFinished:[tesseract recognizedText]];
//        [self performSelectorOnMainThread:@selector(ocrProcessingFinished:)
//                               withObject:[tesseract recognizedText]
//                            waitUntilDone:NO];
//        [NSThread detachNewThreadSelector:@selector(ocrProcessingFinished:) toTarget:self withObject:[tesseract recognizedText]];
    }
    else
        [self performSelectorOnMainThread:@selector(ocrProcessingFinished:)
                               withObject:[tesseract recognizedText]
                            waitUntilDone:NO];    
//    dispatch_async(dispatch_get_main_queue(), ^{
////        [self performSelector:@selector(ocrProcessingFinished:) withObject:[tesseract recognizedText]];
//        [self ocrProcessingFinished:[tesseract recognizedText]];
//    });
    
    //[self ocrProcessingFinished:[tesseract recognizedText]];
    
    
    //NSLog(@"recognize text is ::%@", [tesseract recognizedText]);
    [tesseract clear];
    image = nil;
    tesseract = nil;
    
}

-(void)ocrProcessingFinished: (NSString *)text{
    //NSLog(@"Processing done for the first part");
    //NSLog(@"the text is ::%@",text);
    [self extractLocationName:text];
}

-(void) processOcrForLowerImage: (UIImage *) image{

    CGSize size;
    size.width=image.size.width;
    size.height=image.size.height;
    NSData *jpegImage = UIImageJPEGRepresentation(image,1.0);
    UIImage *newImage = [[UIImage alloc] initWithData:jpegImage];
    
    image  = [imageProcessor resizeImage:newImage toSize:size];
    newImage = nil;
    image = [imageProcessor doBinarize:image];

    
    
    
    Tesseract *tesseract = [[Tesseract alloc] initWithDataPath:@"tessdata" language:@"eng"];
    [tesseract setVariableValue:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.$()#=-:,!" forKey:@"tessedit_char_whitelist"];
    [tesseract setImage:image]; //image to check
    [tesseract recognize];
//    [self performSelectorOnMainThread:@selector(ocrProcessingFinishedLowerImage:)
//                           withObject:[tesseract recognizedText]
//                        waitUntilDone:NO];
//    
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
////        [self performSelector:@selector(ocrProcessingFinishedLowerImage:) withObject:[tesseract recognizedText]];
//        
//        [self ocrProcessingFinishedLowerImage:[tesseract recognizedText]];
//        
//    });
    
    if (SYSTEM_VERSION_GREATER_THAN(@"7.1.2")) {
        [self ocrProcessingFinishedLowerImage:[tesseract recognizedText]];//fine
        //    [self performSelectorOnMainThread:@selector(ocrProcessingFinishedLowerImage:)
        //                           withObject:[tesseract recognizedText]
        //                        waitUntilDone:NO];
        
        
    }
    else
        [self performSelectorOnMainThread:@selector(ocrProcessingFinishedLowerImage:)
                                    withObject:[tesseract recognizedText]
                                 waitUntilDone:NO];
    
    
    //NSLog(@"%@", [tesseract recognizedText]);
    [tesseract clear];
    
    image = nil;
    tesseract = nil;
    
}

-(void)ocrProcessingFinishedLowerImage: (NSString *)text{
    NSLog(@"Processing done for lower part");
   // [NSThread detachNewThreadSelector:@selector(processOcrForAddress:) toTarget:self withObject:addressImage];
    [self extractTotal:text];
}

-(void) processOcrForAddress: (UIImage *) image{
    
    CGSize size;
    
    size.width=image.size.width;
    size.height=image.size.height;
    NSData *jpegImage = UIImageJPEGRepresentation(image,1.0);
    UIImage *newImage = [[UIImage alloc] initWithData:jpegImage];
    
    image  = [imageProcessor resizeImage:newImage toSize:size];
    newImage = nil;
    image = [imageProcessor doBinarize:image];
    
    Tesseract *tesseract = [[Tesseract alloc] initWithDataPath:@"tessdata" language:@"eng"];
    [tesseract setVariableValue:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.$()#=-:,!" forKey:@"tessedit_char_whitelist"];
    [tesseract setImage:image]; //image to check
    [tesseract recognize];
//    [self performSelectorOnMainThread:@selector(ocrProcessingFinishedAddress:)
//                           withObject:[tesseract recognizedText]
//                        waitUntilDone:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
//        [self performSelector:@selector(ocrProcessingFinishedAddress:) withObject:[tesseract recognizedText]];
        [self ocrProcessingFinishedAddress:[tesseract recognizedText]];
    });
    
    //NSLog(@"%@", [tesseract recognizedText]);
    [tesseract clear];
    
    image = nil;
    tesseract = nil;
    
}

-(void)ocrProcessingFinishedAddress: (NSString *)text{
    //NSLog(@"Processing done for lower part");
    [self extractAddress:text];
}



-(void)extractLocationName:(NSString*)text
{
    //NSLog(@"The vendor name text string is :: %@",text);
    
    /*
    // umair changing
    
    NSArray *lines1 = [text componentsSeparatedByString:@"\n"];
    NSEnumerator *nse1 = [lines1 objectEnumerator];
    NSString *tmp1;
    int i=1;
    while(tmp1 = [nse1 nextObject])
    {
        NSLog(@"The %d :: %@",i,tmp1);
        i++;
    }
     */
    
    NSString *string = text;
    NSCharacterSet *separator = [NSCharacterSet newlineCharacterSet];
    NSArray *stringComponents = [string componentsSeparatedByCharactersInSet:separator];
    
    //vendorName =[stringComponents objectAtIndex:0];   //waqar1
    
//    if([self filterVendorPattern:vendorName]){    //waqar1            uncomment
//      vendorName = @"Unrecognized";
//    }
    
    //NSLog(@"Vendor name : %@",vendorName);
    [self extractAddress:text];
    
}

-(void)extractAddress:(NSString*)text{
    
    NSString *string = text;
    
    NSCharacterSet *separator = [NSCharacterSet newlineCharacterSet];
    NSArray *stringComponents = [string componentsSeparatedByCharactersInSet:separator];
    if([stringComponents count]>4){
    address =[NSString stringWithFormat:@"%@ %@ %@",[stringComponents objectAtIndex:1],[stringComponents objectAtIndex:2],[stringComponents objectAtIndex:3]];
    }else
        address = @"Not Found";
    
    //NSLog(@"Address For Lookup : %@",address);
//    [NSThread detachNewThreadSelector:@selector(processOcrForLowerImage:) toTarget:self withObject:totalAndTaxImage];
    


    
//    If you have an NSThread object whose thread is currently running, one way you can send messages to that thread is to use the performSelector:onThread:withObject:waitUntilDone: method of almost any object in your application. Support for performing selectors on threads (other than the main thread) was introduced in OS X v10.5 and is a convenient way to communicate between threads. (This support is also available in iOS.) The messages you send using this technique are executed directly by the other thread as part of its normal run-loop processing. (Of course, this does mean that the target thread has to be running in its run loop; see Run Loops.) You may still need some form of synchronization when you communicate this way, but it is simpler than setting up communications ports between the threads.
//        

    
    
    
    if (SYSTEM_VERSION_GREATER_THAN(@"7.1.2")) {
      [self processOcrForLowerImage:totalAndTaxImage];
      
        
//        [NSThread detachNewThreadSelector:@selector(processOcrForLowerImage:) toTarget:self withObject:totalAndTaxImage];
    }
    else
        [NSThread detachNewThreadSelector:@selector(processOcrForLowerImage:) toTarget:self withObject:totalAndTaxImage];

    
}


-(void)extractTotal:(NSString*)text{
    
    NSString *string = text;
    NSString *object;
    NSString *key;
    
    NSCharacterSet *newLineSeparator = [NSCharacterSet newlineCharacterSet];
    NSCharacterSet *digitsSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789.,"];
    NSArray *stringComponents = [string componentsSeparatedByCharactersInSet:newLineSeparator];
    NSMutableDictionary *keyValDic = [[NSMutableDictionary alloc] init];
    NSArray *tempDataArray;
    total = 0.0;
    
    //NSLog(@"%@",stringComponents);
    
    
    for (int i = 0; i<[stringComponents count]; i++) {
        
        tempDataArray = [NSArray new];
        tempDataArray= [[stringComponents objectAtIndex:i] componentsSeparatedByCharactersInSet:digitsSet];
         //NSLog(@"%@",tempDataArray);
        

        object =[[[[stringComponents objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsSeparatedByCharactersInSet:
                  [digitsSet invertedSet]]
                 componentsJoinedByString:@""];

        key =[[[[stringComponents objectAtIndex:i] componentsSeparatedByCharactersInSet:
                digitsSet ]
               componentsJoinedByString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

        object =[object
                 stringByReplacingOccurrencesOfString:@"," withString:@""];
        object =[object
                 stringByReplacingOccurrencesOfString:@"$" withString:@""];
        object = [object stringByTrimmingCharactersInSet:
                  [NSCharacterSet characterSetWithCharactersInString:@"."]];
        
        NSNumber *objectNumber = [NSNumber numberWithFloat:[object floatValue]];
      
        if([self matchPattern:[key lowercaseString]]){
            [keyValDic removeAllObjects];
            [keyValDic setObject:objectNumber  forKey:key];
            total = [object floatValue];
            break;
        }
      
    }
    
    
    //NSLog(@" Total is %f",total);
    
    // Tax Extraction
    
    if (!isManual) {
        [self extractTax:text];
    }
    else {
        if(isCancled == NO){
            if (SYSTEM_VERSION_GREATER_THAN(@"7.1.2")) {
                sleep(1.5);
                [waitForOCR dismissWithClickedButtonIndex:0 animated:YES];
            }
            else {
                [waitForOCR dismissWithClickedButtonIndex:0 animated:YES];
            }
            [self performSegueWithIdentifier:@"presentResults" sender:nil];
        }else {
             isCancled = YES;
        }
    }
}

-(BOOL) matchPattern:(NSString*)sentence{
    
    
    if ([sentence rangeOfString:@"sub"].location == NSNotFound) {

    NSArray *wordlist = @[@"total",@"grand total",@"tot",@"tota",@"7otal",@"tut",@"amount",@"Amount",@"payable",@"tulul",@"payment",@"otal"];
    
    for (int i =0; i<[wordlist count]; i++) {
        
        if ([sentence rangeOfString:[wordlist objectAtIndex:i]].location != NSNotFound)
        {
            //NSLog(@"Yes it does contain the patten of total");
            return YES;
        }
    }
    }
    return NO;
}


-(void)extractTax:(NSString*)text
{    
    NSString *string = text;
    NSString *object;
    NSString *key;
    
    NSCharacterSet *newLineSeparator = [NSCharacterSet newlineCharacterSet];
    NSCharacterSet *digitsSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789.,"];
    NSArray *stringComponents = [string componentsSeparatedByCharactersInSet:newLineSeparator];
    NSMutableDictionary *keyValDic = [[NSMutableDictionary alloc] init];
    NSArray *tempDataArray;
   
    tax = 0.0;
    
    for (int i = 0; i<[stringComponents count]; i++)
    {
        tempDataArray = [NSArray new];
        tempDataArray= [[stringComponents objectAtIndex:i] componentsSeparatedByCharactersInSet:digitsSet];
        
        object =[[[[stringComponents objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsSeparatedByCharactersInSet:
                  [digitsSet invertedSet]]
                 componentsJoinedByString:@""];
        key =[[[[stringComponents objectAtIndex:i] componentsSeparatedByCharactersInSet:
                digitsSet ]
               componentsJoinedByString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        object =[object
                 stringByReplacingOccurrencesOfString:@"," withString:@""];
        object =[object
                 stringByReplacingOccurrencesOfString:@"$" withString:@""];
        object = [object stringByTrimmingCharactersInSet:
                                        [NSCharacterSet characterSetWithCharactersInString:@"."]];
        NSNumber *objectNumber = [NSNumber numberWithInt:[object floatValue]];
        
        if([self matchTaxPattern:[key lowercaseString]]){
            [keyValDic removeAllObjects];
            [keyValDic setObject:objectNumber  forKey:key];
            tax = [object floatValue];
            break;
        }
    }
    
    //NSLog(@" Tax  is %f",tax);
    if(isCancled == NO){
        
        if (SYSTEM_VERSION_GREATER_THAN(@"7.1.2")) {
            sleep(1.5);
            [waitForOCR dismissWithClickedButtonIndex:0 animated:YES];
        }
        else
            [waitForOCR dismissWithClickedButtonIndex:0 animated:YES];
        
    
        //[ActivityIndicatorBox hideActivityIndicatorforView:self.view];
        [self performSegueWithIdentifier:@"presentResults" sender:nil];
        
//        [ActivityIndicatorBox showActivityIndicatorWithLabel:@"processing...." forView:self.view];
        
    }else{
    
        isCancled = YES;
    }
}

-(BOOL) matchTaxPattern:(NSString*)sentence{
    
    NSArray *wordlist = @[@"tax",@"gst",@"tx",@"ax"];
    
    for (int i =0; i<[wordlist count]; i++) {
        if ([sentence rangeOfString:[wordlist objectAtIndex:i]].location != NSNotFound) {
            //NSLog(@"Yes it does contains the pattern of tax");
            return YES;
        }
    }
    return NO;
}

-(BOOL) filterVendorPattern:(NSString*)sentence{
    
//    NSArray *wordlist = @[@"@",@"%",@".",@"!",@"(",@")",@"{",@"}",@",",@"[",@"]",@":",@"?",@"/",@";",@"+",@"=",@"*",@"#",@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    
    NSArray *wordlist = @[@"@",@"%",@"!",@"(",@")",@"{",@"}",@",",@"[",@"]",@":",@"?",@"/",@";",@"+",@"=",@"*",@"#"];
    
    for (int i =0; i<[wordlist count]; i++) {
        if ([sentence rangeOfString:[wordlist objectAtIndex:i]].location != NSNotFound) {
            //NSLog(@"Vendor name does contain Gibberish characters");
            return YES;
        }
    }
    return NO;
}

- (IBAction)backTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc
{
    if (SYSTEM_VERSION_GREATER_THAN(@"7.1.2"))
    {
        @try {
            [imageCropper removeObserver:self forKeyPath:@"crop"];
        }
        @catch (NSException *exception) {
            //do nthing
        }
    }
    else{
          //donothing
    }
    

   
}
@end
