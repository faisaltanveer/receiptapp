//
//  InfoViewController.m
//  Tax Scanner//
//  Created by Inam Ur Rahman on 4/25/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import "InfoViewController.h"
#import "ViewController.h"

#define IS_IPHONE_5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)


@interface InfoViewController ()

@end

@implementation InfoViewController

@synthesize totalReceipts,imageSizeInBytes;

BOOL isOnTheOtherPage = NO;
NSString *embedHTML;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
float memoryUsed;
float picSize;
int memoryTotal;
int totalR;
float imageSize;
float fileSize;

float totalPicsSize;

static void getMemoryStatus () {
    
    
    
//    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
//    if (appDelegate.firstRun)
//    {
    
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        //picSize = imageSize/1048576;
    
        fileSize = [defaults floatForKey:@"fileSize"];
        //NSLog(@"fileSize : %.2f",fileSize);
        //NSLog(@"picSize : %.2f",imageSize);
        //fileSize = fileSize+imageSize;
        
    
        //memoryUsed = (fileSize)/1048576;
        
        [defaults setFloat:fileSize forKey:@"fileSize"];

    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    array = [defaults objectForKey:@"arrayOfPicSizes"];
    
    if([array count]==0)
    {
        totalPicsSize = 0.00;
    }
    else
    {        
        for(int i=0 ; i< [array count] ; i++)
        {
            totalPicsSize = totalPicsSize + [[array objectAtIndex:i]intValue] ;
        }

    }
    fileSize = fileSize + totalPicsSize;
    memoryUsed = (fileSize)/1048576;
    totalPicsSize = totalPicsSize/1048576;

    //NSLog(@"fileSize after : %.2f",fileSize+totalPicsSize);
    //NSLog(@"Total image size is %f ",totalPicsSize);
    

   
    
    
//    }else
//    {
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        
//        picSize = imageSize/1048576;
//        
//        
//        fileSize = [defaults floatForKey:@"fileSize"];
//        NSLog(@"fileSize : %.2f",fileSize);
//        NSLog(@"picSize : %.2f",picSize);
//        fileSize = fileSize+imageSize;
//        
//        NSLog(@"fileSize after : %.2f",fileSize);
//        memoryUsed = (fileSize)/1048576;
//        
//        [defaults setFloat:fileSize forKey:@"fileSize"];
//        
//    }
    
    
    
    
    
    
//    NSString *folderPath = [[NSBundle mainBundle] bundlePath];
//    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
//    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
//    NSString *fileName;
//    fileSize = 0.0;
//    
//    while (fileName = [filesEnumerator nextObject]) {
//        NSDictionary *fileDictionary = [[NSFileManager defaultManager] fileAttributesAtPath:[folderPath stringByAppendingPathComponent:fileName] traverseLink:YES];
//        fileSize += [fileDictionary fileSize];
//    }
//    
//    NSLog(@"App size : %f",fileSize+(imageSize*totalR));
//    
//    picSize = (float)imageSize/1048576;
//    memoryUsed = (fileSize+(imageSize))/1048576;
    
    
    
    
    /////
    
//    mach_port_t host_port;
//    mach_msg_type_number_t host_size;
//    vm_size_t pagesize;
//    
//    host_port = mach_host_self();
//    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
//    host_page_size(host_port, &pagesize);
//    
//    vm_statistics_data_t vm_stat;
//    
//    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
//        NSLog(@"Failed to fetch vm statistics");
//    
//    /* Stats in bytes */
//    memoryUsed = (vm_stat.active_count +
//                          vm_stat.inactive_count +
//                          vm_stat.wire_count) * pagesize;
//    memoryFree = vm_stat.free_count * pagesize;
//    memoryTotal = memoryUsed + memoryFree;
//    
//    memoryUsed/=(1048576);
//    memoryFree/=(1048576);
//    memoryTotal/=(1048576);
//    NSLog(@"In MBbbbs used: %u free: %u total: %u", memoryUsed, memoryFree, memoryTotal);
}


- (void)updateStatus
{
	vm_statistics_data_t vmStats;
	mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
	host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
    
	const int totalPages = vmStats.wire_count + vmStats.active_count +
    vmStats.inactive_count + vmStats.free_count;
	const int availablePages = vmStats.free_count;
	const int activePages = vmStats.active_count;
	const int wiredPages = vmStats.wire_count;
	const int purgeablePages = vmStats.purgeable_count;
    
	NSMutableString* txt = [[NSMutableString alloc] initWithCapacity:512];
	[txt appendFormat:@"Total: %d (%.2dMB)", totalPages, pagesToMB(totalPages)];
	[txt appendFormat:@"nAvailable: %d (%.2dMB)", availablePages, pagesToMB(availablePages)];
	[txt appendFormat:@"nActive: %d (%.2dMB)", activePages, pagesToMB(activePages)];
	[txt appendFormat:@"nWired: %d (%.2dMB)", wiredPages, pagesToMB(wiredPages)];
	[txt appendFormat:@"nPurgeable: %d (%.2dMB)", purgeablePages, pagesToMB(purgeablePages)];
    
	//NSLog(@"txt - %@",txt);
	
}

int pagesToMB(int var)
{
    return (vm_page_size * var)/(1024 * 1024);
}

int getAvailableMemoryInKB()
{
    vm_statistics_data_t vmStats;
    mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
    kern_return_t kernReturn = host_statistics(mach_host_self(),
                                               HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
    if(kernReturn != KERN_SUCCESS)
        return -1;
    return (vm_page_size * vmStats.free_count) / 1024;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [_infoWebView setDelegate:self];
    [_infoWebView setScalesPageToFit:NO];
    
    totalPicsSize = 0.0;
    
    totalR = totalReceipts;
    
    imageSize = imageSizeInBytes;
    
//    NSLog(@"Image Size is %i",imageSizeInBytes);
//    NSLog(@"totalR %i",totalReceipts);
    
    getMemoryStatus();
    
    [availableMemoryLbl setText:[NSString stringWithFormat:@"%.2f MB",totalPicsSize]]; // picture size
    
    [totalMemoryLbl setText:[NSString stringWithFormat:@"%d MB",memoryTotal]];
    [usedMemoryLbl setText:[NSString stringWithFormat:@"%.2f MB",memoryUsed]];
    
    embedHTML =[NSString stringWithFormat:@"<html><body><h2 style='font-family:ArialRoundedMTBold;'>General:</h2><p style='font-family:ArialRoundedMTBold;'  align='left'>Thank you for purchasing the Zonnins Sales Tax Scanner ©. If this app doesn't save you at least $200 on your next tax return, we'll refund your money. We’d like our users to know that we do not sell or collect any of your personal purchase information like many free receipt scanners. Please note the information on this website does not represent personal tax advice either express or implied. You are encouraged to seek professional tax advice for personal income tax questions. We are continuously updating our software with the most sophisticated OCR technology. Please email support@zonnins.com to provide any feedback.</p><h2 style='font-family:ArialRoundedMTBold;'>Why This Works:</h2><p style='font-family:ArialRoundedMTBold;'  align='left'>The IRS allows individuals to deduct their state income tax or their sales tax in any given year. However, the IRS hopes most people are not going to keep all of their receipts so they provide standard sales tax deductions based on how much income you make. To see what your allowed sales tax deduction is <a href ='http://www.irs.gov/Individuals/Sales-Tax-Deduction-Calculator'>click here</a>. As you can imagine, this is a conservative estimate. Because the IRS allows the use of receipts of images, we've created the Zonnins Sales Tax App ©. Using the Zonnins Sales Tax App, skip the hassle of saving all of your receipts and adding them up at year end.</br></br>Page 9 of this <a href = 'http://www.irs.gov/pub/irs-irbs/irb97-13.pdf'>IRS document</a> discusses the legality of using electronically saved receipts. Unfortunately, the IRS doesn't allow taxpayers to use their credit card reports as evidence of receipts. Also, don’t forget to print out those online receipts and save them. Now take a picture of those receipts and start saving!</p></br></body></html>"];
    [_infoWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.zonnins.com"]]];
    //[_infoWebView loadHTMLString:embedHTML baseURL:nil];
    
    if(IS_IPHONE_5){
        
        CGRect newFrame = CGRectMake(0, 91, 326, 363);
        [_infoWebView setFrame:newFrame];
        [_infoWebView reload];
        
        CGRect memLbl = CGRectMake(20, 469, 115, 21);
        [_memoryLbl setFrame:memLbl];
        
        CGRect appSizLbl = CGRectMake(30, 498, 83, 21);
        [_appSizeLbl setFrame:appSizLbl];
        
        CGRect picSizLbl = CGRectMake(30, 527, 83, 21);
        [_picSizeLbl setFrame:picSizLbl];
        
        CGRect usdMemLbl = CGRectMake(146, 497, 154, 21);
        [usedMemoryLbl setFrame:usdMemLbl];
        
        CGRect avMemLbl = CGRectMake(153, 526, 154, 21);
        [availableMemoryLbl setFrame:avMemLbl];
    }
    else{
        NSLog(@"Not Iphone 5");
    }
}



-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [_infoWebView setScalesPageToFit:YES];
        isOnTheOtherPage = YES;
        return YES;
    }
    
    return YES;
}
- (void) webViewDidFinishLoad:(UIWebView *)webView {
    
    NSString* js =
    @"var meta = document.createElement('meta'); "
    "meta.setAttribute( 'name', 'viewport' ); "
    "meta.setAttribute( 'content', 'width = device-width' ); "
    "document.getElementsByTagName('head')[0].appendChild(meta)";
    
    [webView stringByEvaluatingJavaScriptFromString: js];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backTapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
//    //NSLog(@"%d",[_infoWebView canGoBack]);
//   
//    [self.delegate setSizeofImageToZero];
//    if (isOnTheOtherPage == YES){
//        [_infoWebView setScalesPageToFit:NO];
//        isOnTheOtherPage = NO;
//        //[_infoWebView loadHTMLString:embedHTML baseURL:nil];
//    }else{
//        [self.navigationController popViewControllerAnimated:YES];
//
//    }
}
@end
