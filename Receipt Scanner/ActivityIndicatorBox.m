//
//  ActivityIndicatorBox.m
//  Amblist-iOS
//
//  Created by CyberDesignz on 11/23/12.
//
//

#import "ActivityIndicatorBox.h"

@implementation ActivityIndicatorBox

+(void)showActivityIndicatorWithLabel:(NSString *)labelText forView:(UIView *)view{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = labelText;
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}

+(void)hideActivityIndicatorforView:(UIView *)view{
    
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MBProgressHUD hideHUDForView:view animated:YES];
    
}

+(BOOL)isActivityIndicatorAddedOnView:(UIView *)view
{
    return  [MBProgressHUD isHudExistsForView:view];
}
@end
