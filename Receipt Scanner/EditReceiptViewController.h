//
//  EditReceiptViewController.h
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 2/6/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ShouldFetchRecordsKey @"ShouldFetch"

@interface EditReceiptViewController : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate,UITextViewDelegate,UIPickerViewDelegate>
{
    NSMutableArray *dataArray;
    UIPickerView *myPickerView ;
}

@property (strong, nonatomic) NSString* editVendor;
@property (strong, nonatomic) NSString* editTotal;
@property (strong, nonatomic) NSString* editTax;
@property (strong, nonatomic) NSString* editId;
@property (strong, nonatomic) NSString* editImage;
@property (strong, nonatomic) NSString* editDate;

@property (strong, nonatomic) NSString* Selected_Category;
@property (strong, nonatomic) NSString* Receipt_Description;
@property (strong, nonatomic) NSString* Sales_Tax_Percantage;



@property (weak, nonatomic) IBOutlet UITextField *vendorNameText;
@property (weak, nonatomic) IBOutlet UITextField *totalText;

@property (weak, nonatomic) IBOutlet UITextField *Category;
@property (nonatomic, retain) IBOutlet UITextView *Description;
@property (weak, nonatomic) IBOutlet UILabel *Category_Label;
@property (weak, nonatomic) IBOutlet UILabel *Description_Label;
@property(nonatomic, retain)IBOutlet UIScrollView * Fields_Scroll;

@property (weak, nonatomic) IBOutlet UITextField *salesTaxPercentageTextField;

@property (weak, nonatomic) IBOutlet UILabel *Vendor_Name_Label;
@property (weak, nonatomic) IBOutlet UILabel *Total;
@property (weak, nonatomic) IBOutlet UILabel *Tax;
@property (weak, nonatomic) IBOutlet UILabel *taxPercLbl;
@property (weak, nonatomic) IBOutlet UILabel *Date;

@property (weak, nonatomic) IBOutlet UILabel *SelectionLabel;


- (IBAction)backTapped:(id)sender;
- (IBAction)doneTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *taxText;
- (IBAction)updatePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *pickerTopView;
@property (strong, nonatomic) IBOutlet UIImageView *receiptImagePreview;
@property (weak, nonatomic) IBOutlet UITextField *dateTextField;
@property (weak, nonatomic) IBOutlet UIView *imageDisplayModalWindow;
@end
