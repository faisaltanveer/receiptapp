//
//  ImageProcessing.h
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/24/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GPUImage.h"

@interface ImageProcessing : NSObject

// Image Processing Functions

-(UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)size;
-(UIImage *) grayImage :(UIImage *)inputImage;
-(UIImage *) invertColors:(UIImage *)inputImage;
-(UIImage *) doBinarize:(UIImage *)sourceImage;
-(UIImage *) applySharpness:(UIImage *)sourceImage;
-(UIImage *) fixRGB:(UIImage *)sourceImage;
-(UIImage *) applyErosion:(UIImage *)sourceImage;
-(UIImage *) applyLaplacian:(UIImage *)sourceImage;
-(UIImage *) applyCannyEdgeDetection:(UIImage *)sourceImage;
-(UIImage *) applyDilation:(UIImage *)sourceImage;
-(UIImage *) applyBrightness:(UIImage *)sourceImage;
-(UIImage*) cropUpperImage:(UIImage*)sourceImage;
-(UIImage*) cropLowerImage:(UIImage*)sourceImage;

@end
