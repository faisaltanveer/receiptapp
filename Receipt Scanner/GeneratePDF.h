//
//  GeneratePDF.h
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 2/4/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GeneratePDF : NSObject
{
    int firstpage;
     float total_tax, total_amount;
     NSDictionary *total_dic;
}
- (NSString*) generatePdfWithFilePath:(NSString *)thefilePath :(NSMutableArray *)dataArray;
@end
