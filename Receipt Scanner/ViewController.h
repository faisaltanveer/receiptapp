//
//  ViewController.h
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/22/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BJImageCropper.h"
#import <TesseractOCR/TesseractOCR.h>
#import "ImageProcessing.h"
#import "AppDelegate.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "InfoViewController.h"

#define ShouldFetchRecordsKey @"ShouldFetch" 

@interface ViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,MFMailComposeViewControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate>

{
    NSMutableArray *dataArray;
    int Selected_Year;
    
    NSMutableString  *Selected_Category_Picker;
    
    NSMutableArray *CategoryArrayForGenerateReport;

    NSMutableArray *arrayOfPicSizes;

}


@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControll;


- (IBAction)SegmentControllAction:(id)sender;


@property (strong, nonatomic)InfoViewController *infoViewobj;
@property (assign, nonatomic) int totalReceipts;
@property (assign, nonatomic) int sizeofimage;
@property (assign, nonatomic) BOOL pictureTaken;

- (IBAction)takePicPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *cropImageFrame;
@property (strong, nonatomic) IBOutlet UILabel *totalSpentLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)generateReport:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *topBarView;

@property (strong, nonatomic) UIImage *binarizedImage;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) NSMutableArray *tableDataSourceArray;
@property (strong, nonatomic) AppDelegate *appDel;
@property (strong, nonatomic) ImageProcessing *imageProcessor;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* tax;
@property (strong, nonatomic) NSString* total;
@property (strong, nonatomic) NSString* recId;
@property (strong, nonatomic) NSString* recDate;

@property (strong, nonatomic) NSString* Selected_Category;
@property (strong, nonatomic) NSString* ReceiptDescription;
@property (strong, nonatomic) NSString* Sales_Tsx_Percentage;


@property (strong, nonatomic) NSString* imageString;
//@property (strong, nonatomic) IBOutlet UILabel *yearLabel;


//need to change
//@property (strong, nonatomic) IBOutlet UILabel *selectedcategorylabel;
@property (strong, nonatomic) IBOutlet UILabel *YearORCategorySelection;




- (IBAction)doneButton:(id)sender;
- (IBAction)infoButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIPickerView *yearPickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *categoryPickerview;

@property (strong, nonatomic) IBOutlet UIView *pickerTopBar;
@property (weak, nonatomic) IBOutlet UILabel *zonninsLabel;

-(void)clearCacheData;
- (IBAction)manualEnterPressed:(id)sender;



@end
