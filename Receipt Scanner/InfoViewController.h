//
//  InfoViewController.h
//  Tax Scanner
//
//  Created by Usman Ahmad on 4/25/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <mach/mach.h>
#import <mach/mach_host.h>
#import "AppDelegate.h"

@protocol InfoViewControllerDelegate <NSObject>
- (void)setSizeofImageToZero;
@end

@interface InfoViewController : UIViewController<UIWebViewDelegate>
{
    __weak IBOutlet UILabel *totalMemoryLbl;   
    __weak IBOutlet UILabel *usedMemoryLbl;
    __weak IBOutlet UILabel *availableMemoryLbl;
    
}


@property (nonatomic, weak) id <InfoViewControllerDelegate> delegate;

- (IBAction)backTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIWebView *infoWebView;
@property (assign, nonatomic) int totalReceipts;
@property (assign, nonatomic) int imageSizeInBytes;

@property (weak, nonatomic) IBOutlet UILabel *memoryLbl;
@property (weak, nonatomic) IBOutlet UILabel *appSizeLbl;
@property (weak, nonatomic) IBOutlet UILabel *picSizeLbl;



@end
