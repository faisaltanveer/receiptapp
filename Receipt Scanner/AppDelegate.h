//
//  AppDelegate.h
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/22/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Receipts_Data.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
}

@property (nonatomic,readwrite) BOOL firstRun;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSString *)applicationDocumentsDirectory;
- (void) insertResults:(Receipts_Data*)dataManager;
- (NSMutableArray*) retrieveAllReciepts;
- (NSMutableArray *) readFilteredVendors:(NSString *)vendorName;
- (void) deleteReceiptWithId:(NSString *)recId;
- (Receipts_Data*) getReceiptWithId:(NSString *)recId;

@property (strong, nonatomic) UIWindow *window;

@end
