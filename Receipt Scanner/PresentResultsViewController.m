//
//  PresentResultsViewController.m
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/31/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import "PresentResultsViewController.h"
#import "AppDelegate.h"
#import "Receipts_Data.h"

@interface PresentResultsViewController ()

@end

@implementation PresentResultsViewController
@synthesize venName,tax,total,recieptImage,isManualRec;
UIDatePicker *datePicker;
UITapGestureRecognizer *tap;
BOOL isPickerTopBarVisible = NO;
UITapGestureRecognizer *tapGesture;

float salesTaxPercent = 0.0;

NSString *previousTaxFieldVal;
NSString *previousTotalFieldVal;
NSString *previousSalesTaxFieldVal;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    
    _vendorNameTextField.delegate = self;
    _totalTextField.delegate = self;
    _totalTextField.delegate = self;
    _dateTextField.delegate = self;
    myPickerView.delegate=self;
    _salesTaxPercentageTextField.delegate = self;
    _taxTextField.delegate = self;
    self.Category.delegate=self;
    //NSLog(@"%@",self.address);
    
    
    
    
    [self.vendorNameTextField setText:venName];
    NSString * stringWithDolla = [NSString stringWithFormat:@"$%@",tax];
    
    //[self.taxTextField setText:stringWithDolla];
//    [self.totalTextField setText:[NSString stringWithFormat:@"$%.2f",[total floatValue] ]];
    
    if([total floatValue]>0)
    {
        [self.totalTextField setText:[NSString stringWithFormat:@"$%.2f",[total floatValue] ]];
    }
    else
    {
        [self.totalTextField setPlaceholder:@"$0.00"];
    }
    
    UILabel *dollarLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    dollarLabel.text = @"$";
    [self.taxTextField setLeftView:dollarLabel];
    [self.taxTextField setLeftViewMode:UITextFieldViewModeAlways];
    
    
    //NSLog(@"totalTextField input %@",self.totalTextField.text);
    
    [self.receiptImage setImage:[self stringToUIImage:[self recieptImage]]];
    NSDate *date  = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"MM/d/yyyy"];
    NSString *startDateString = [dateFormat stringFromDate:date];
    _dateTextField.text = startDateString;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    _salesTaxPercentageTextField.leftView = paddingView;
    _salesTaxPercentageTextField.leftViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    _vendorNameTextField.leftView = paddingView;
    _vendorNameTextField.leftViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    _totalTextField.leftView = paddingView;
    _totalTextField.leftViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    _dateTextField.leftView = paddingView;
    _dateTextField.leftViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    _taxTextField.leftView = paddingView;
    _taxTextField.leftViewMode = UITextFieldViewModeAlways;
    
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    self.Category.leftView = paddingView;
    self.Category.leftViewMode = UITextFieldViewModeAlways;
    
    
    tapGesture= [[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(dismissKeyboard)];
    
    
    self.Description.delegate=self;
}

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    
    
    
    [self.navigationController setNavigationBarHidden:YES];
    
    //waqar
    
    _vendorNameTextField.tag = 1000;
    
    arrayOfPicSizes = [[NSMutableArray alloc]init];
    
    //NSLog(@"Image Size in result view is %d",_imageSizeInBytes);
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    
    
    
    
    _taxTextField.inputAccessoryView = numberToolbar;
    _totalTextField.inputAccessoryView = numberToolbar;
    _salesTaxPercentageTextField.inputAccessoryView = numberToolbar;
    
    ///
    
    dataArray = [[NSMutableArray alloc]init];
    
    self.Category.text=@"Personal";
    
    [dataArray addObject:@"Business"];
    [dataArray addObject:@"Personal"];
    [dataArray addObject:@"Other"];
    
    self.Description.backgroundColor = [UIColor clearColor];
    
    myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    
    myPickerView.frame=datePicker.frame;
    
    self.Category.inputView=myPickerView;
    
    
    self.Fields_Scroll.contentSize=CGSizeMake(320, 420);
    if([UIScreen mainScreen].bounds.size.height == 480)
        self.Fields_Scroll.contentSize=CGSizeMake(320,460);
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setMaximumDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    _dateTextField.tag = 70;
    _dateTextField.inputView = datePicker;
    
    
    self.Description.delegate = self;
    
    
    // Do any additional setup after loading the view.
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"receiptNumber"]) {
        NSLog(@"Key found");
        
    }else{
        NSLog(@"Key not found");
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"receiptNumber"];
        [[NSUserDefaults standardUserDefaults] setFloat:0.0 forKey:@"totalAmount"];
        [[NSUserDefaults standardUserDefaults] setFloat:0.0 forKey:@"totalSalesTax"];
    }
    
    if (isManualRec == NO) {
        
        
        // Double Checking the total value
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Tax : $%@",tax] message:@"Is it the right tax?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        alertView.tag = 2;
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        [[alertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeDecimalPad];
        [alertView textFieldAtIndex:0].text =[NSString stringWithFormat:@"$%.2f",[tax floatValue]]; //waqar
        
        [alertView show];
        
    }
    
    else{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Tax"
                                                            message:@"Please enter in sales tax" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        alertView.tag = 2;
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
//        [alertView textFieldAtIndex:0].text =@"$"; //waqar
        
        [[alertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeDecimalPad];  
        
        [[alertView textFieldAtIndex:0] setPlaceholder:@"$0.00"];
        [alertView show];
        
        //waqar
        [_taxTextField setText:[NSString stringWithFormat:@"$%.2f",[tax floatValue] ]];
        ///
        
        //[_taxTextField setPlaceholder:@"$0.00"];  //Enter Tax in Dollar   waqar
        [_vendorNameTextField setPlaceholder:@"Enter Vendor"];
        [_totalTextField setPlaceholder:@"Enter Total"];
        
        if ([tax floatValue]>0.0 && [total floatValue] > 0.0)
        {
            float t = [tax floatValue];
            float tot = [total floatValue];
            
            salesTaxPercent = (t/(tot-t))*100;    // previous (t/(tot))*100;
            
            // salesTaxPercent = ([tax floatValue]/([_totalTextField.text floatValue] - [tax floatValue]))*100;
            //            salesTaxPercent = ([tax floatValue]/([_totalTextField.text floatValue] ))*100;
            
            _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"%.2f",salesTaxPercent];
        }
    }
    
    tap= [[UITapGestureRecognizer alloc] initWithTarget:self
                                                 action:@selector(dismissKeyboard)];
    
    // [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"receiptNumber"];
    
}

//waqar

-(void)cancelNumberPad{
    
    [_taxTextField resignFirstResponder];
    [_totalTextField resignFirstResponder];
    [_salesTaxPercentageTextField resignFirstResponder];
    
    _taxTextField.text = previousTaxFieldVal;
    _totalTextField.text = previousTotalFieldVal;
    _salesTaxPercentageTextField.text = previousSalesTaxFieldVal;
    
    //NSLog(@"Previous tax %@",previousTaxFieldVal);
    //NSLog(@"Previous total %@",previousTotalFieldVal);
    //NSLog(@"Previous sales tax %@",previousSalesTaxFieldVal);
}

-(void)doneWithNumberPad
{
    
    [_taxTextField resignFirstResponder];
    [_totalTextField resignFirstResponder];
    [_salesTaxPercentageTextField resignFirstResponder];
    
    tax = _taxTextField.text ;
    total = _totalTextField.text;  //before
    
    NSString *TAX = [tax stringByReplacingOccurrencesOfString:@"$" withString:@""];
    NSString *TOTAL = [total stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    float t = [TAX floatValue];
    float tot = [TOTAL floatValue];
    /*
    float chk4minustax = t;
    float chk4minustotal = tot;
    
    if(chk4minustax > (chk4minustotal/2))
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Warning"] message:@"Sales Tax value must be half or less than total amount" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
        alertView.alertViewStyle = UIAlertViewStyleDefault;
        [alertView show];
        _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"0.00"];
    }
    else*/
    {
        _taxTextField.text = [NSString stringWithFormat:@"$%.2f",t];
        _totalTextField.text = [NSString stringWithFormat:@"$%.2f",tot];
        if (t > 0.0 && tot > 0.0) {
            // salesTaxPercent = ([tax floatValue]/([_totalTextField.text floatValue] - [tax floatValue]))*100;
            salesTaxPercent = (t/(tot-t))*100;
            
            _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"%.2f",salesTaxPercent];
        }else
            _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"0.00"];
    }
    
    
    //NSLog(@"Tax is %@",TAX);
    //NSLog(@"Total is %@",TOTAL);
    
    
    
    
    
    
    
}


///

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = 3;
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [dataArray objectAtIndex: row];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.Category.text=[dataArray objectAtIndex: row];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard {
    isPickerTopBarVisible = NO;
    [_pickerViewTopBar setHidden:YES];
    [_dateTextField resignFirstResponder];
    //  [myPickerView resignFirstResponder];
    [self.Category resignFirstResponder];
}

-(void)datePickerValueChanged:(id)sender{
    
    
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"MM/d/yyyy";
    
    //NSLog(@"%@", [format stringFromDate:[(UIDatePicker*)sender date]]);
    _dateTextField.text =[format stringFromDate:[(UIDatePicker*)sender date]];
    
    //NSLog(@"Date Changed %@",[(UIDatePicker*)sender date]);
    
}
- (IBAction)doneTapped:(id)sender
{
    isPickerTopBarVisible = NO;
    [_pickerViewTopBar setHidden:YES];
    [_dateTextField resignFirstResponder];
    [self.Category resignFirstResponder];
    [myPickerView resignFirstResponder];
}

- (IBAction)saveReceiptPressed:(id)sender
{
    if (([_taxTextField.text length] > 0) && ([_dateTextField.text length] > 0) &&  ([_Category.text length] > 0))
    {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        arrayOfPicSizes = [[defaults objectForKey:@"arrayOfPicSizes"] mutableCopy];
        
        if(!arrayOfPicSizes)
        {
            arrayOfPicSizes = [[NSMutableArray alloc]init];
        }
        
        NSNumber *number = [NSNumber numberWithInt:_imageSizeInBytes];
        [arrayOfPicSizes addObject:number];
        [defaults setObject:arrayOfPicSizes forKey:@"arrayOfPicSizes"];
        [defaults synchronize];
        
        
        AppDelegate *appDel = [[AppDelegate alloc] init];
        
        Receipts_Data *receipt =[[Receipts_Data alloc] init];
        
        //  [NSEntityDescription insertNewObjectForEntityForName:@"Receipts_Data" inManagedObjectContext:[appDel managedObjectContext]];
        
        receipt.tax = [self.taxTextField.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
        
        //waqar
        receipt.total = [self.totalTextField.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
        
        //receipt.total = self.totalTextField.text;
        receipt.vendor_name = self.vendorNameTextField.text;
        receipt.receipt_image = self.recieptImage;
        receipt.receipt_number =[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"receiptNumber"]];
        receipt.receipt_date = _dateTextField.text;
        receipt.receipt_description=self.Description.text;
        receipt.selected_category=self.Category.text;
        receipt.tax_percent = [NSString stringWithFormat:@"%.2f",[self.salesTaxPercentageTextField.text floatValue]];
        
        //receipt.sales_tax = self.salesTaxPercentageTextField.text;
        
        NSLog(@"SalesTaxPerCentage %@ ",receipt.tax_percent);
        
        
        // receipt.
        
        //receipt.selected_category=@"dasdbhabsdjabsdja";
        
        
        NSString *totalTextFieldText = [self.totalTextField.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
        
        NSString *taxTextFieldText = [self.taxTextField.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
        
        
        float totalVal =[[NSUserDefaults standardUserDefaults] floatForKey:@"totalAmount"]+[totalTextFieldText floatValue];
        
        float totalSalesTax = [[NSUserDefaults standardUserDefaults] floatForKey:@"totalSalesTax"]+[taxTextFieldText floatValue];
        //awais
        /*
        float chk4minustax = [taxTextFieldText floatValue];
        float chk4minustotal = [totalTextFieldText floatValue];
        
        if(chk4minustax > (chk4minustotal/2))
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Cannot Save Receipt"] message:@"Sales Tax value must be half or less than total amount" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
            alertView.alertViewStyle = UIAlertViewStyleDefault;
            [alertView show];
        }
        else*/
        {
            [appDel insertResults:receipt];
            
            [[NSUserDefaults standardUserDefaults] setFloat:totalVal forKey:@"totalAmount"];
            [[NSUserDefaults standardUserDefaults] setFloat:totalSalesTax forKey:@"totalSalesTax"];
            
            //NSLog(@"totalVal %.2f",totalVal);
            //NSLog(@"totalSalesTax %.2f",totalSalesTax);
            
            int val =[[NSUserDefaults standardUserDefaults] integerForKey:@"receiptNumber"]+1;
            [[NSUserDefaults standardUserDefaults] setInteger:val forKey:@"receiptNumber"];
            
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"scrolled"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Receipt Added"] message:@"Receipt Has Been Added Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
            alertView.tag = 3;
            alertView.alertViewStyle = UIAlertViewStyleDefault;*/
            
            
            //   Sound Effect On Save Receipt.
            
            OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
            [audio playEffect:@"Dome.wav"];  //
            
            [self dismissViewControllerAnimated:NO completion:nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
            //[alertView show];
            
            [[NSUserDefaults standardUserDefaults] setObject:[[NSNumber alloc]initWithBool:YES] forKey:ShouldFetchRecordsKey];
        }
        
        
        
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Cannot Save Receipt"] message:@"Sales Tax, Date and Category cannot be empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
        alertView.alertViewStyle = UIAlertViewStyleDefault;
        [alertView show];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    //NSLog(@"textViewDidEndEditing:");
    [textView resignFirstResponder];
    [self.Description resignFirstResponder];
}


- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    //NSLog(@"textViewShouldEndEditing:");
    textView.backgroundColor = [UIColor clearColor];
    [self.Description resignFirstResponder];
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    //NSLog(@"textViewShouldBeginEditing:");
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    //NSLog(@"textViewDidBeginEditing:");
    
    [self.Fields_Scroll setContentOffset:CGPointMake(0, textView.frame.origin.y - 145) animated:YES];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (IBAction)backPressed:(id)sender
{
    //waqar
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    NSMutableArray *array = [[NSMutableArray alloc] init];
//    array = [[defaults objectForKey:@"arrayOfPicSizes"]mutableCopy];
//    
//    int lastIndex = [array count]-1;
//    [array removeObjectAtIndex:lastIndex];
//    
//    [defaults setObject:array forKey:@"arrayOfPicSizes"];
//    [defaults synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [alertView cancelButtonIndex]) {
        NSLog(@"The cancel button was clicked for alertView");
        [self.taxTextField setText:[NSString stringWithFormat:@"$%.2f",0.00]];
    }
    
    if (alertView.tag == 2 && buttonIndex == 1)
    {
        
        UITextField * alertTextField = [alertView textFieldAtIndex:0];
        //NSLog(@"alerttextfiled - %@",alertTextField.text);
        NSString * alertTaxText = alertTextField.text;
        
        
        if(alertTaxText.length > 0 && [alertTaxText characterAtIndex:0] != '$'){
            [self.taxTextField setText:[NSString stringWithFormat:@"$%.2f",[alertTaxText floatValue]]];
        }
        else if (alertTaxText.length > 0 && [alertTaxText characterAtIndex:0] == '$'){
            NSString *taxwithoutdollar = [alertTaxText
                                          stringByReplacingOccurrencesOfString:@"$" withString:@""];
            
            [self.taxTextField setText:[NSString stringWithFormat:@"$%.2f",[taxwithoutdollar floatValue] ]];
        }
        else{
            [self.taxTextField setText:[NSString stringWithFormat:@"$%.2f",0.00]];
        }
        
        //[self.taxTextField setText:alertTaxText];
        
        tax = _taxTextField.text ;
        total = _totalTextField.text;  //before
        
        NSString *TAX = [tax stringByReplacingOccurrencesOfString:@"$" withString:@""];
        NSString *TOTAL = [total stringByReplacingOccurrencesOfString:@"$" withString:@""];
        
        float t = [TAX floatValue];
        float tot = [TOTAL floatValue];
        
        
        //NSLog(@"Tax is %@",TAX);
        //NSLog(@"Total is %@",TOTAL);
        
        
        if (t > 0.0 && tot > 0.0) {
            // salesTaxPercent = ([tax floatValue]/([_totalTextField.text floatValue] - [tax floatValue]))*100;
            salesTaxPercent = (t/(tot-t))*100;    // previous (t/(tot))*100;
            
            _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"%.2f",salesTaxPercent];
        }else
            _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"0.00"];
        
        
        
        ///neeed to change
        /*if ([tax floatValue]>0.0 && [total floatValue] > 0.0)
        {
            float t = [tax floatValue];
            float tot = [total floatValue];
            
            salesTaxPercent = (t/(tot-t))*100;    // previous (t/(tot))*100;
            
            // salesTaxPercent = ([tax floatValue]/([_totalTextField.text floatValue] - [tax floatValue]))*100;
//            salesTaxPercent = ([tax floatValue]/([_totalTextField.text floatValue] ))*100;
            
            _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"%.2f",salesTaxPercent];
        }*/
    }
    else if (alertView.tag == 3 && buttonIndex == 0)
    {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (UIImage *)stringToUIImage:(NSString *)string
{
    NSData *data = [self base64DataFromString:string];
    
    return [UIImage imageWithData:data];
}


- (NSData *)base64DataFromString: (NSString *)string
{
    unsigned long ixtext, lentext;
    unsigned char ch, inbuf[4], outbuf[3];
    short i, ixinbuf;
    Boolean flignore, flendtext = false;
    const unsigned char *tempcstring;
    NSMutableData *theData;
    
    if (string == nil)
    {
        return [NSData data];
    }
    
    ixtext = 0;
    
    tempcstring = (const unsigned char *)[string UTF8String];
    
    lentext = [string length];
    
    theData = [NSMutableData dataWithCapacity: lentext];
    
    ixinbuf = 0;
    
    while (true)
    {
        if (ixtext >= lentext)
        {
            break;
        }
        
        ch = tempcstring [ixtext++];
        
        flignore = false;
        
        if ((ch >= 'A') && (ch <= 'Z'))
        {
            ch = ch - 'A';
        }
        else if ((ch >= 'a') && (ch <= 'z'))
        {
            ch = ch - 'a' + 26;
        }
        else if ((ch >= '0') && (ch <= '9'))
        {
            ch = ch - '0' + 52;
        }
        else if (ch == '+')
        {
            ch = 62;
        }
        else if (ch == '=')
        {
            flendtext = true;
        }
        else if (ch == '/')
        {
            ch = 63;
        }
        else
        {
            flignore = true;
        }
        
        if (!flignore)
        {
            short ctcharsinbuf = 3;
            Boolean flbreak = false;
            
            if (flendtext)
            {
                if (ixinbuf == 0)
                {
                    break;
                }
                
                if ((ixinbuf == 1) || (ixinbuf == 2))
                {
                    ctcharsinbuf = 1;
                }
                else
                {
                    ctcharsinbuf = 2;
                }
                
                ixinbuf = 3;
                
                flbreak = true;
            }
            
            inbuf [ixinbuf++] = ch;
            
            if (ixinbuf == 4)
            {
                ixinbuf = 0;
                
                outbuf[0] = (inbuf[0] << 2) | ((inbuf[1] & 0x30) >> 4);
                outbuf[1] = ((inbuf[1] & 0x0F) << 4) | ((inbuf[2] & 0x3C) >> 2);
                outbuf[2] = ((inbuf[2] & 0x03) << 6) | (inbuf[3] & 0x3F);
                
                for (i = 0; i < ctcharsinbuf; i++)
                {
                    [theData appendBytes: &outbuf[i] length: 1];
                }
            }
            
            if (flbreak)
            {
                break;
            }
        }
    }
    
    return theData;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    tax = _taxTextField.text ;
    total = _totalTextField.text;  //before
    
    NSString *TAX = [tax stringByReplacingOccurrencesOfString:@"$" withString:@""];
    NSString *TOTAL = [total stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    float t = [TAX floatValue];
    float tot = [TOTAL floatValue];
    
    
    //NSLog(@"Tax is %@",TAX);
    //NSLog(@"Total is %@",TOTAL);
    
    
    if (t > 0.0 && tot > 0.0) {
        // salesTaxPercent = ([tax floatValue]/([_totalTextField.text floatValue] - [tax floatValue]))*100;
        salesTaxPercent = (t/(tot-t))*100;    // previous (t/(tot))*100;
        
        _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"%.2f",salesTaxPercent];
    }else
        _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"0.00"];
    
    if (textField.tag==90210)
    {
        self.Fields_Scroll.contentSize=CGSizeMake(320, 420);
        if([UIScreen mainScreen].bounds.size.height == 480)
            self.Fields_Scroll.contentSize=CGSizeMake(320,460);
    }
    
    
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    previousTaxFieldVal = _taxTextField.text;
    previousTotalFieldVal = _totalTextField.text;
    previousSalesTaxFieldVal = _salesTaxPercentageTextField.text;
    
    
    if (textField.tag==90210)
    {
        /*  self.Fields_Scroll.contentSize=CGSizeMake(320, self.Fields_Scroll.contentSize.height+100);
         
         self.Fields_Scroll.contentOffset=CGPointMake(0, 380);
         */
    }
    return YES;
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(void)animateUp:(float)upFactor{
    /*
    CGRect Frame = self.view.frame;
    Frame.origin.y = self.view.frame.origin.y - upFactor;
    
    
    [UIView animateWithDuration:0.3
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.view.frame = Frame;
                     }
                     completion:nil];
    
    */
    
}

-(void)animateDown:(float)downFactor{
    /*
    CGRect Frame = self.view.frame;
    Frame.origin.y = self.view.frame.origin.y + downFactor;
    
    
    [UIView animateWithDuration:0.3
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.view.frame = Frame;
                     }
                     completion:nil];
    
    */
    //    NSFileManager *fM = [NSFileManager defaultManager];
    //    fileList = [[fM directoryContentsAtPath:DOCUMENTS_FOLDER] retain];
    //    NSMutableArray *directoryList = [[NSMutableArray alloc] init];
    //    for(NSString *file in fileList) {
    //        NSString *path = [DOCUMENTS_FOLDER stringByAppendingPathComponent:file];
    //        BOOL isDir = NO;
    //        [fM fileExistsAtPath:path isDirectory:(&isDir)];
    //        if(isDir) {
    //            [directoryList addObject:file];
    //        }
    //    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //waqar
    
    NSString *Text = [[NSString alloc] initWithFormat:@"%@%@",textField.text,string];
    
    if([Text length] > 1)
        return YES;
    
    if(textField.text.length == 1 && [textField.text isEqualToString:@"$"] && textField.tag != 1000)
    {
        return NO;
    }
    
    return YES;
    
    ///
}

- (void) hideView
{
    [UIView animateWithDuration:0.5
                     animations:^{
                         datePicker.frame = CGRectMake(0, -250, 320, 50);
                     } completion:^(BOOL finished) {
                         [datePicker removeFromSuperview];
                     }];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField.tag == 10) {
        
        isPickerTopBarVisible = NO;
        [_pickerViewTopBar setHidden:YES];
        
        [self animateUp:8.0];
        
    }else if (textField.tag == 20){
        
        isPickerTopBarVisible = NO;
        [_pickerViewTopBar setHidden:YES];
        
        [self animateUp:40.0];
        
    }else if (textField.tag == 30){
        
        isPickerTopBarVisible = NO;
        [_pickerViewTopBar setHidden:YES];
        
        [self animateUp:80.0];
        
    }else if (textField.tag == 70)
    {
        self.SelectionLabel.text=@"Select Date";
        
        if (isPickerTopBarVisible == NO) {
            isPickerTopBarVisible = NO;
            [_pickerViewTopBar setHidden:NO];
            
        }else{
            
            isPickerTopBarVisible = NO;
            [_pickerViewTopBar setHidden:YES];
            
        }
        
        [self.view addGestureRecognizer:tap];
        
    }
    else if (textField.tag == 90210) {
        
        self.SelectionLabel.text=@"Select Category";
        if (isPickerTopBarVisible == NO) {
            
            isPickerTopBarVisible = NO;
            [_pickerViewTopBar setHidden:NO];
            
        }else{
            
            isPickerTopBarVisible = NO;
            [_pickerViewTopBar setHidden:YES];
            
        }
        
        [self.view addGestureRecognizer:tap];
        
    }
    else{
        isPickerTopBarVisible = NO;
        [_pickerViewTopBar setHidden:YES];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField.tag == 10) {
        
        [self animateDown:8.0];
        
    }else if (textField.tag == 20){
        
        [self animateDown:40.0];
        
    }else if (textField.tag == 30){
        
        [self animateDown:80.0];
        
    }else if (textField.tag == 70){
        [self.view removeGestureRecognizer:tap];
        
    }
    
    
    
}


@end
