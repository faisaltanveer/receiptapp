//
//  ImageProcessing.m
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/24/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import "ImageProcessing.h"

@implementation ImageProcessing



-(UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)size
{
    float width = size.width;
    float height = size.height;
    
    UIGraphicsBeginImageContext(size);
    CGRect rect = CGRectMake(0, 0, width, height);
    
    [image drawInRect: rect];
    
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    
    return smallImage;
}



- (UIImage *) grayImage :(UIImage *)inputImage
{
    
    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:inputImage];
    GPUImageGrayscaleFilter *greyScale = [[GPUImageGrayscaleFilter alloc] init];
    [imageSource addTarget:greyScale];
    [imageSource processImage];
    
    UIImage *retImage = [greyScale imageFromCurrentlyProcessedOutput];
    return retImage;
}

- (UIImage *) invertColors:(UIImage *)inputImage
{
    
    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:inputImage];
    GPUImageColorInvertFilter *invertFilter = [[GPUImageColorInvertFilter alloc] init];
    [imageSource addTarget:invertFilter];
    [imageSource processImage];
    
    UIImage *retImage = [invertFilter imageFromCurrentlyProcessedOutput];
    return retImage;
}

-(UIImage *) doBinarize:(UIImage *)sourceImage
{
    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:sourceImage];
    
    GPUImageAdaptiveThresholdFilter *stillImageFilter = [[GPUImageAdaptiveThresholdFilter alloc] init];

   
    GPUImageLuminanceThresholdFilter *lumianceFilter = [[GPUImageLuminanceThresholdFilter alloc] init];
     lumianceFilter.threshold = 0.5f;
  

    
    [stillImageFilter prepareForImageCapture];
    [lumianceFilter prepareForImageCapture];
    
    [imageSource addTarget:lumianceFilter];
    [imageSource addTarget:stillImageFilter];
    [imageSource processImage];
    
    UIImage *retImage = [stillImageFilter imageFromCurrentlyProcessedOutput];
    return retImage;
}

-(UIImage *) applyBrightness:(UIImage *)sourceImage
{
  
    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:sourceImage];
    GPUImageBrightnessFilter *brightFilter = [[GPUImageBrightnessFilter alloc] init];
    brightFilter.brightness = 0.05;
    [imageSource addTarget:brightFilter];
    [imageSource processImage];
    UIImage *retImage = [brightFilter imageFromCurrentlyProcessedOutput];
    
    
    
    return retImage;


}


-(UIImage *) applyDilation:(UIImage *)sourceImage
{
    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:sourceImage];
    GPUImageDilationFilter *stillImageFilter = [[GPUImageDilationFilter alloc] init];

    [stillImageFilter setupFilterForSize:CGSizeMake(0.01, 0.01)];
    [stillImageFilter prepareForImageCapture];
    [imageSource addTarget:stillImageFilter];
    
    [imageSource processImage];
    
    UIImage *retImage = [stillImageFilter imageFromCurrentlyProcessedOutput];
    return retImage;
}




-(UIImage *) applySharpness:(UIImage *)sourceImage{
    
    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:sourceImage];
    GPUImageSharpenFilter *sharpFilter = [[GPUImageSharpenFilter alloc] init];
    sharpFilter.sharpness = 0.1;

    [imageSource addTarget:sharpFilter];
    [imageSource processImage];
    UIImage *retImage = [sharpFilter imageFromCurrentlyProcessedOutput];
    
    return retImage;
    
    
}


-(UIImage *) fixRGB:(UIImage *)sourceImage{
    
    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:sourceImage];
    GPUImageRGBFilter *rgbFilters = [[GPUImageRGBFilter alloc] init];
    [imageSource addTarget:rgbFilters];
    [imageSource processImage];
    UIImage *retImage = [rgbFilters imageFromCurrentlyProcessedOutput];
    
    return retImage;
    
    
}



-(UIImage *) applyErosion:(UIImage *)sourceImage{
    
    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:sourceImage];
    GPUImageErosionFilter *erosionFilter = [[GPUImageErosionFilter alloc] init];
    [erosionFilter setupFilterForSize:CGSizeMake(0.04, 0.04)];

    [imageSource addTarget:erosionFilter];
    [imageSource processImage];
    UIImage *retImage = [erosionFilter imageFromCurrentlyProcessedOutput];
    
    return retImage;
    
}
-(UIImage *) applyLaplacian:(UIImage *)sourceImage{
    
    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:sourceImage];
    GPUImageLaplacianFilter *lapFilter = [[GPUImageLaplacianFilter alloc] init];
    
    [imageSource addTarget:lapFilter];
    [imageSource processImage];
    UIImage *retImage = [lapFilter imageFromCurrentlyProcessedOutput];
    
    return retImage;
}

-(UIImage *) applyCannyEdgeDetection:(UIImage *)sourceImage{
    
    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:sourceImage];
    GPUImageCannyEdgeDetectionFilter *cannyEdge = [[GPUImageCannyEdgeDetectionFilter alloc] init];
    [cannyEdge setUpperThreshold:0.2];
    
    [imageSource addTarget:cannyEdge];
    [imageSource processImage];
    UIImage *retImage = [cannyEdge imageFromCurrentlyProcessedOutput];
    
    return retImage;
    
    
}
-(UIImage*) cropUpperImage:(UIImage*)sourceImage{

    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:sourceImage];
    GPUImageCropFilter *cropUpper = [[GPUImageCropFilter alloc] initWithCropRegion:CGRectMake(0, 0, 1, 0.5)];
    [imageSource addTarget:cropUpper];
    [imageSource processImage];
    UIImage *retImage = [cropUpper imageFromCurrentlyProcessedOutput];
    
    return retImage;


}


-(UIImage*) cropLowerImage:(UIImage*)sourceImage{
    
    GPUImagePicture *imageSource = [[GPUImagePicture alloc] initWithImage:sourceImage];
    GPUImageCropFilter *cropLower = [[GPUImageCropFilter alloc] initWithCropRegion:CGRectMake(0, 0.5, 1, 0.5)];
    
    [imageSource addTarget:cropLower];
    [imageSource processImage];
    UIImage *retImage = [cropLower imageFromCurrentlyProcessedOutput];
    
    return retImage;
    
    
}



@end
