//
//  GeneratePDF.m
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 2/4/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import "GeneratePDF.h"
#import "Receipts_Data.h"


@implementation GeneratePDF

int pageHeight;
int imagePlacement;
int textPlacement;

- (NSString*) generatePdfWithFilePath:(NSString *)thefilePath :(NSMutableArray *)dataArray
{
    //NSLog(@"generatePdfWithFilePath - %@",thefilePath);
    UIGraphicsBeginPDFContextToFile(thefilePath, CGRectZero, nil);
    //UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 50, 100), nil);
       NSData *data;
    pageHeight = 792;
    imagePlacement = 0;
    textPlacement = 100;
    
    total_tax = 0;
    total_amount = 0;
    
    for (int i=0; i<[dataArray count]; i++) {
        total_dic = [self check_for_totals:[dataArray objectAtIndex:i]];
    }
//
//    //draw totals
//    
//    [self drawtotal:total_dic];
    
    for (int i = 0; i< [dataArray count]; i++) {
             UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 612, 792), nil);

        // Drawing Text
        
        [self drawText:[dataArray objectAtIndex:i]];
        
        //Drawing Image
        data = [self base64DataFromString:[(Receipts_Data*)[dataArray objectAtIndex:i] receipt_image]];
        
        
        [self drawImage:[UIImage imageWithData:data]];
    }
    // Closing context and returning the file path.
    UIGraphicsEndPDFContext();
    return thefilePath;
}


-(void)drawtotal : (NSDictionary *)dic
{
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(currentContext, 0.0, 0.0, 0.0, 1.0);
    int pageIntend = 0;
    
    
    NSString *text = [[NSString alloc] initWithFormat:@"    Total Sales Tax $ : $%@         Total Amount $ : $%@\n",[dic valueForKey:@"Tax"], [dic valueForKey:@"Amount"]];
    
    NSLog(@"drawText - %@",text);
    
    UIFont *font = [UIFont systemFontOfSize:14.0];
//    CGSize stringSize = [text sizeWithFont:font
//                         constrainedToSize:CGSizeMake(612-4, 792-4)
//                             lineBreakMode:UILineBreakModeWordWrap];
    
    CGRect renderingRect = CGRectMake(2, 70, 612, 792);
    [text drawInRect:renderingRect
            withFont:font
       lineBreakMode:UILineBreakModeWordWrap
           alignment:UITextAlignmentLeft];
    pageIntend++;
}

-(NSDictionary *) check_for_totals:(Receipts_Data*) receipt
{
    NSString * salesTax     = [receipt tax];
    NSString * total        = [receipt total];
    
    total_tax = [salesTax floatValue]+total_tax;
    total_amount = [total floatValue]+total_amount;
    
    NSDictionary *dic;
    
    dic =        [NSDictionary dictionaryWithObjectsAndKeys:
                 [NSString stringWithFormat:@"%0.2f", total_amount], @"Amount",
                 [NSString stringWithFormat:@"%0.2f", total_tax], @"Tax", nil];

    return dic;
}




- (void) drawImage:(UIImage*)image
{
    
    NSData *jpegImage = UIImageJPEGRepresentation(image,0.0);
    UIImage *newImage = [[UIImage alloc] initWithData:jpegImage];
    
    
    [newImage drawInRect:CGRectMake( (692 - newImage.size.width/2)/6, 260, newImage.size.width/4, newImage.size.height/4)];
    //[image drawInRect:CGRectMake( (692 - image.size.width/2)/6, 200, image.size.width/4, image.size.height/4)];
}

- (UIImage *)stringToUIImage:(NSString *)string
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:string
                                                      options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}




- (NSData *)base64DataFromString: (NSString *)string
{
    unsigned long ixtext, lentext;
    unsigned char ch, inbuf[4], outbuf[3];
    short i, ixinbuf;
    Boolean flignore, flendtext = false;
    const unsigned char *tempcstring;
    NSMutableData *theData;
    
    if (string == nil)
    {
        return [NSData data];
    }
    
    ixtext = 0;
    
    tempcstring = (const unsigned char *)[string UTF8String];
    
    lentext = [string length];
    
    theData = [NSMutableData dataWithCapacity: lentext];
    
    ixinbuf = 0;
    
    while (true)
    {
        if (ixtext >= lentext)
        {
            break;
        }
        
        ch = tempcstring [ixtext++];
        
        flignore = false;
        
        if ((ch >= 'A') && (ch <= 'Z'))
        {
            ch = ch - 'A';
        }
        else if ((ch >= 'a') && (ch <= 'z'))
        {
            ch = ch - 'a' + 26;
        }
        else if ((ch >= '0') && (ch <= '9'))
        {
            ch = ch - '0' + 52;
        }
        else if (ch == '+')
        {
            ch = 62;
        }
        else if (ch == '=')
        {
            flendtext = true;
        }
        else if (ch == '/')
        {
            ch = 63;
        }
        else
        {
            flignore = true;
        }
        
        if (!flignore)
        {
            short ctcharsinbuf = 3;
            Boolean flbreak = false;
            
            if (flendtext)
            {
                if (ixinbuf == 0)
                {
                    break;
                }
                
                if ((ixinbuf == 1) || (ixinbuf == 2))
                {
                    ctcharsinbuf = 1;
                }
                else
                {
                    ctcharsinbuf = 2;
                }
                
                ixinbuf = 3;
                
                flbreak = true;
            }
            
            inbuf [ixinbuf++] = ch;
            
            if (ixinbuf == 4)
            {
                ixinbuf = 0;
                
                outbuf[0] = (inbuf[0] << 2) | ((inbuf[1] & 0x30) >> 4);
                outbuf[1] = ((inbuf[1] & 0x0F) << 4) | ((inbuf[2] & 0x3C) >> 2);
                outbuf[2] = ((inbuf[2] & 0x03) << 6) | (inbuf[3] & 0x3F);
                
                for (i = 0; i < ctcharsinbuf; i++)
                {
                    [theData appendBytes: &outbuf[i] length: 1];
                }
            }
            
            if (flbreak)
            {
                break;
            }
        }
    }
    
    return theData;
}

- (void) drawText:(Receipts_Data*) receipt
{
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(currentContext, 0.0, 0.0, 0.0, 1.0);
    int pageIntend = 0;
    
    if(firstpage==0)
    {
        [self drawtotal:total_dic];
        firstpage=1;
    }
    
    
      
    NSString * salesTax     = [receipt tax];
    NSString * salesTaxPerc = [receipt tax_percent];
    NSString * total        = [receipt total];
    NSString * catergory    = [receipt selected_category];
    NSString * date         = [receipt receipt_date];
    NSString * vendorName   = [receipt vendor_name];
    NSString * desc         = [receipt receipt_description];
    
    total_tax = [salesTax floatValue]+total_tax;
    total_amount = [total floatValue]+total_amount;
    
    
    //NSLog(@"SalesTaxpercentage - %.2f",[salesTaxPerc floatValue]);
    
    
        NSString *text = [[NSString alloc] initWithFormat:@"Sales Tax $ : $%@ \nDate : %@\nCategory : %@\nTotal $ : $%@\nSales Tax %% : %@ \nVendor Name: %@ \nDescription : %@ \n",salesTax,date,catergory,total,salesTaxPerc,vendorName ,desc];
    
    NSLog(@"drawText - %@",text);
    
    UIFont *font = [UIFont systemFontOfSize:14.0];
        CGSize stringSize = [text sizeWithFont:font
                             constrainedToSize:CGSizeMake(612-4, 792-4)
                                 lineBreakMode:UILineBreakModeWordWrap];
        
        CGRect renderingRect = CGRectMake(2, 100, 612, 792);
        [text drawInRect:renderingRect
                withFont:font
           lineBreakMode:UILineBreakModeWordWrap
               alignment:UITextAlignmentLeft];
        pageIntend++;
}

@end
