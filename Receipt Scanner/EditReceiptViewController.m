//
//  EditReceiptViewController.m
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 2/6/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import "EditReceiptViewController.h"
#import "AppDelegate.h"
@interface EditReceiptViewController ()

@end

@implementation EditReceiptViewController
@synthesize editTax,editTotal,editVendor,editId,editImage,editDate,Receipt_Description,Selected_Category,Sales_Tax_Percantage;


UITapGestureRecognizer *tap;
BOOL isFullScreen;
CGRect prevFrame;
UIDatePicker *datePicker;
UITapGestureRecognizer *tapGest;
UITapGestureRecognizer *tapGestForModal;
UIPinchGestureRecognizer *pinchGest;
UIPanGestureRecognizer *panGest;

BOOL isPickerViewVisible = NO;

NSString *originalTaxFieldVal;
NSString *originalTotalFieldVal;
NSString *originalSalesTaxFieldVal;

NSString *previousTaxFieldVal;
NSString *previousTotalFieldVal;
NSString *previousSalesTaxFieldVal;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //waqar
    
    _vendorNameText.tag = 1000;
    
    
    previousTaxFieldVal = @"";
    previousTotalFieldVal = @"";
    previousSalesTaxFieldVal = @"";
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    
    _taxText.inputAccessoryView = numberToolbar;
    _totalText.inputAccessoryView = numberToolbar;
    _salesTaxPercentageTextField.inputAccessoryView = numberToolbar;
    
    self.Description.backgroundColor = [UIColor clearColor];
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    
    //self.Vendor_Name_Label.attributedText = [[NSAttributedString alloc] initWithString:@"Sales Tax %:"attributes:underlineAttribute];
    //waqar
    self.Vendor_Name_Label.attributedText = [[NSAttributedString alloc] initWithString:@"Vendor Name:"attributes:underlineAttribute];
    ///
    
    self.Total.attributedText = [[NSAttributedString alloc] initWithString:@"Total:"attributes:underlineAttribute];
    self.Tax.attributedText = [[NSAttributedString alloc] initWithString:@"Sales Tax"attributes:underlineAttribute];
    self.Date.attributedText = [[NSAttributedString alloc] initWithString:@"Date:"attributes:underlineAttribute];
    self.Category_Label.attributedText = [[NSAttributedString alloc] initWithString:@"Category:"attributes:underlineAttribute];
    self.taxPercLbl.attributedText = [[NSAttributedString alloc] initWithString:@"Sales Tax%:"attributes:underlineAttribute];
    self.Description_Label.attributedText = [[NSAttributedString alloc] initWithString:@"Description:"attributes:underlineAttribute];
    
    // Setting up delegates
    [self.navigationController setNavigationBarHidden:YES];
    _vendorNameText.delegate = self;
    _taxText.delegate = self;
    _totalText.delegate = self;
    
    _salesTaxPercentageTextField.delegate = self; //waqar
    
    _dateTextField.delegate = self;
    
    self.Category.delegate=self;
    
    self.Description.delegate = self;
    
    self.Fields_Scroll.contentSize=CGSizeMake(320,440);
    if([UIScreen mainScreen].bounds.size.height == 480)
        self.Fields_Scroll.contentSize=CGSizeMake(320,480);
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [datePicker setMaximumDate:[NSDate date]];
    _dateTextField.tag = 70;
    _dateTextField.inputView = datePicker;
    dataArray = [[NSMutableArray alloc]init];
    
    [dataArray addObject:@"Business"];
    [dataArray addObject:@"Personal"];
    [dataArray addObject:@"Other"];
    
    
    myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    myPickerView.frame=datePicker.frame;
    
    self.Category.inputView=myPickerView;
    
    // Adding the padding to the Textfields
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _vendorNameText.leftView = paddingView;
    _vendorNameText.leftViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _taxText.leftView = paddingView;
    _taxText.leftViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _totalText.leftView = paddingView;
    _totalText.leftViewMode = UITextFieldViewModeAlways;
    
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _dateTextField.leftView = paddingView;
    _dateTextField.leftViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    self.Category.leftView = paddingView;
    self.Category.leftViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    self.salesTaxPercentageTextField.leftView = paddingView;
    self.salesTaxPercentageTextField.leftViewMode = UITextFieldViewModeAlways;

    
    //Assigning the values to the variables
    _vendorNameText.text = editVendor;  //waqar
    
    self.salesTaxPercentageTextField.text= Sales_Tax_Percantage;
    _taxText.text = [NSString stringWithFormat:@"$%@",editTax];
    
        
    if (editTotal == nil || [editTotal isKindOfClass:[NSNull class]] || [editTotal isEqualToString:@""])
    {
        _totalText.text = @"$0.00";  //waqar
    }
    else
         _totalText.text = [NSString stringWithFormat:@"$%@",editTotal];  //waqar
    
    _dateTextField.text = editDate;
    
    self.Description.text=Receipt_Description;
    
    self.Category.text=Selected_Category;
    
    [_receiptImagePreview setImage:[UIImage imageWithData:[self base64DataFromString:editImage]]];
    
    [_receiptImagePreview setUserInteractionEnabled:YES];
    
    isFullScreen = false;
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgToFullScreen)];
    tap.delegate = self;
    [_receiptImagePreview addGestureRecognizer:tap];
    
    tapGest= [[UITapGestureRecognizer alloc] initWithTarget:self
                                                     action:@selector(dismissKeyboard)];
    
    
    //    pinchGest = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    //    pinchGest.delegate = self;
    
    // panGest = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    //panGest.delegate = self;
    
    tapGestForModal = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgToFullScreen)];
    tapGestForModal.delegate = self;
    [_imageDisplayModalWindow addGestureRecognizer:tapGestForModal];
    
}

//waqar

-(void)cancelNumberPad{
    
    [_taxText resignFirstResponder];
    [_totalText resignFirstResponder];
    [_salesTaxPercentageTextField resignFirstResponder];
    
    
    //    NSLog(@"originalTaxFieldVal %@",originalTaxFieldVal);
    //    NSLog(@"originalTotalFieldVal %@",originalTotalFieldVal);
    //    NSLog(@"originalSalesTaxFieldVal %@",originalSalesTaxFieldVal);
    
    //    if(previousTaxFieldVal != originalTaxFieldVal && ![previousTaxFieldVal isEqual:@""]){
    //        _taxText.text = previousTaxFieldVal;
    //    }else{
    //        _taxText.text = originalTaxFieldVal;
    //    }
    //
    //    if(previousTotalFieldVal != originalTotalFieldVal && ![previousTotalFieldVal isEqual:@""]){
    //        _totalText.text = previousTotalFieldVal;
    //    }else{
    //        _totalText.text = originalTotalFieldVal;
    //    }
    //
    //    if(previousSalesTaxFieldVal != originalSalesTaxFieldVal && ![previousSalesTaxFieldVal isEqual:@""]){
    //        _salesTaxPercentageTextField.text = previousSalesTaxFieldVal;
    //    }else{
    //        _salesTaxPercentageTextField.text = originalSalesTaxFieldVal;
    //    }
    
    _taxText.text = previousTaxFieldVal;
    _totalText.text = previousTotalFieldVal;
    _salesTaxPercentageTextField.text = previousSalesTaxFieldVal;
    
    NSLog(@"Previous tax %@",previousTaxFieldVal);
    NSLog(@"Previous total %@",previousTotalFieldVal);
    NSLog(@"Previous sales tax %@",previousSalesTaxFieldVal);
    
}

-(void)doneWithNumberPad{
    
    
    [_taxText resignFirstResponder];
    [_totalText resignFirstResponder];
    [_salesTaxPercentageTextField resignFirstResponder];
    
    
    NSString *tax = _taxText.text ;
    NSString *total = _totalText.text;
    
    
    NSString *TAX = [tax stringByReplacingOccurrencesOfString:@"$" withString:@""];
    NSString *TOTAL = [total stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    float t = [TAX floatValue];
    float tot = [TOTAL floatValue];
    
    //awais
    
    /*
    float chk4minustax = t;
    float chk4minustotal = tot;
    
    if(chk4minustax > (chk4minustotal/2))
    {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Warning"] message:@"Sales Tax value must be half or less than total amount" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
        alertView.alertViewStyle = UIAlertViewStyleDefault;
        [alertView show];
        _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"0.00"];
        _taxText.text = [NSString stringWithFormat:@"0.00"];
        
        
    }
    else*/
    {
        _taxText.text = [NSString stringWithFormat:@"$%.2f",t];
        _totalText.text = [NSString stringWithFormat:@"$%.2f",tot];
        
        if (t > 0.0 && tot > 0.0) {
            float salesTaxPercent = (t/(tot-t))*100;
            _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"%.2f",salesTaxPercent];
        }else
            _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"0.00"];
        
        previousTaxFieldVal = _taxText.text;
        previousTotalFieldVal = _totalText.text;
        previousSalesTaxFieldVal = _salesTaxPercentageTextField.text;
        
    }
    
    //NSLog(@"Tax is %@",TAX);
    //NSLog(@"Total is %@",TOTAL);
    
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    originalTaxFieldVal = _taxText.text;
    originalTotalFieldVal = _totalText.text;
    originalSalesTaxFieldVal = _salesTaxPercentageTextField.text;
    
    previousTaxFieldVal = _taxText.text;
    previousTotalFieldVal = _totalText.text;
    previousSalesTaxFieldVal = _salesTaxPercentageTextField.text;
    
    return YES;
}

///


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = 3;
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [dataArray objectAtIndex: row];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //NSLog(@"You selected this: %@", [dataArray objectAtIndex: row]);
    self.Category.text=[dataArray objectAtIndex:row];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.Description.delegate = self;
    //NSLog(@"Description %@",self.Description);
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)dismissKeyboard {
    isPickerViewVisible = NO;
    [_pickerTopView setHidden:YES];
    [_dateTextField resignFirstResponder];
    [self.Category resignFirstResponder];
    [myPickerView resignFirstResponder];
}


- (void)handlePinch:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    
    pinchGestureRecognizer.view.transform = CGAffineTransformScale(pinchGestureRecognizer.view.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
    pinchGestureRecognizer.scale = 1;
}
- (void)handlePan:(UIPanGestureRecognizer *)panGestureRecognizer
{
    CGPoint translation = [panGestureRecognizer translationInView:self.view];
    panGestureRecognizer.view.center = CGPointMake(panGestureRecognizer.view.center.x + translation.x,
                                                   panGestureRecognizer.view.center.y + translation.y);
    [panGestureRecognizer setTranslation:CGPointMake(0, 0) inView:self.view];
}

//image zooming

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch;
{
    BOOL shouldReceiveTouch = YES;
    
    if (gestureRecognizer == tap) {
        shouldReceiveTouch = (touch.view == _receiptImagePreview);
    }
    else if ([gestureRecognizer isMemberOfClass:[UISwipeGestureRecognizer class]]) {
        gestureRecognizer.enabled = NO;
        // Return NO for views that don't support Swipes
    }
    
    return shouldReceiveTouch;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //waqar
    
    NSString *Text = [[NSString alloc] initWithFormat:@"%@%@",textField.text,string];
    
    if([Text length] > 1)
        return YES;
    
    if(textField.text.length == 1 && [textField.text isEqualToString:@"$"] && textField.tag != 1000)
    {
        return NO;
    }
    
    return YES;
    
    ///
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField.tag == 70) {
        
        self.SelectionLabel.text=@"Select Date";
        
        if (isPickerViewVisible == NO) {
            
            isPickerViewVisible = NO;
            [_pickerTopView setHidden:NO];
            
        }else{
            
            isPickerViewVisible = NO;
            [_pickerTopView setHidden:YES];
            
        }
        
        [self.view addGestureRecognizer:tapGest];
        [_receiptImagePreview setUserInteractionEnabled:NO];
        
    }
    else if (textField.tag == 90210)
    {
        self.SelectionLabel.text=@"Select Category";
        if (isPickerViewVisible == NO)
        {
            isPickerViewVisible = NO;
            [_pickerTopView setHidden:NO];
        }
        else
        {
            isPickerViewVisible = NO;
            [_pickerTopView setHidden:YES];
        }
        
        [self.view addGestureRecognizer:tapGest];
        [_receiptImagePreview setUserInteractionEnabled:NO];
    }
    else
    {
        isPickerViewVisible = NO;
        [_pickerTopView setHidden:YES];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField.tag == 70) {
        [self.view removeGestureRecognizer:tapGest];
        
        [_receiptImagePreview setUserInteractionEnabled:YES];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    //NSLog(@"textViewDidBeginEditing:");
    [self.Fields_Scroll setContentOffset:CGPointMake(0, textView.frame.origin.y - 125) animated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    //NSLog(@"textViewDidEndEditing:");
    [textView resignFirstResponder];
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    //NSLog(@"textViewShouldEndEditing:");
    textView.backgroundColor = [UIColor clearColor];
    return YES;
}

-(void)datePickerValueChanged:(id)sender{
    
    
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"MM/d/yyyy";
    
    //NSLog(@"%@", [format stringFromDate:[(UIDatePicker*)sender date]]);
    _dateTextField.text =[format stringFromDate:[(UIDatePicker*)sender date]];
    
    //NSLog(@"Date Changed %@",[(UIDatePicker*)sender date]);
    
}


-(void)imgToFullScreen{
    if (!isFullScreen) {
        
        [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
            //save previous frame
            prevFrame = _receiptImagePreview.frame;
            [_receiptImagePreview setFrame:[[UIScreen mainScreen] bounds]];
        }completion:^(BOOL finished){
            isFullScreen = TRUE;
            [_imageDisplayModalWindow setHidden:NO];
            //            [_receiptImagePreview addGestureRecognizer:panGest];
            //            [_receiptImagePreview addGestureRecognizer:pinchGest];
            
        }];
        return;
    }
    else{
        
        
        [_imageDisplayModalWindow setHidden:YES];
        
        [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
            [_receiptImagePreview setFrame:prevFrame];
            //            [_receiptImagePreview removeGestureRecognizer:panGest];
            //            [_receiptImagePreview removeGestureRecognizer:pinchGest];
        }completion:^(BOOL finished){
            isFullScreen = FALSE;
            
            
            
        }];
        return;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)updatePressed:(id)sender {
    
    
    
    if (([_taxText.text length] > 0) && ([_dateTextField.text length] > 0) &&  ([_Category.text length] > 0))
    {
        /* empty - do something */
        NSLog(@"I am not empty");
        
        
        //Setting up variables to accress CoreData.
        AppDelegate *appDel = [AppDelegate new];
        Receipts_Data *newReceipt = [Receipts_Data new];
        
        
        // Calculating the Sales Tax percentage
        //        float salesTaxPercent = ([_taxText.text floatValue]/([_totalText.text floatValue]))*100;
        
        //Updating the values in the core data
        [newReceipt setVendor_name:_vendorNameText.text];
        [newReceipt setTax:[_taxText.text stringByReplacingOccurrencesOfString:@"$" withString:@""]];
        
        [newReceipt setTotal:[_totalText.text stringByReplacingOccurrencesOfString:@"$" withString:@""]];
        
        //[newReceipt setTotal:_totalText.text];
        [newReceipt setReceipt_image:editImage];
        [newReceipt setReceipt_number:editId];
        [newReceipt setTax_percent:[NSString stringWithFormat:@"%.2f",[self.salesTaxPercentageTextField.text floatValue]]];
        [newReceipt setReceipt_date:_dateTextField.text];
        [newReceipt setSelected_category:self.Category.text];
        [newReceipt setReceipt_description:self.Description.text];
        
        
        [appDel deleteReceiptWithId:editId];
        [appDel insertResults:newReceipt];
        //Updating the values in the userdefaults
        [self updateUserDefaults];
        
        
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Cannot Update Receipt"] message:@"Sales Tax, Date and Category cannot be empty" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
        
        alertView.alertViewStyle = UIAlertViewStyleDefault;
        
        [alertView show];
    }
    
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.vendorNameText resignFirstResponder];
    [self.Description resignFirstResponder];
    //    [textField resignFirstResponder];
    //
    //
    //    NSString *tax = _taxText.text ;
    //    NSString *total = _totalText.text;
    //
    //    NSString *TAX = [tax stringByReplacingOccurrencesOfString:@"$" withString:@""];
    //    NSString *TOTAL = [total stringByReplacingOccurrencesOfString:@"$" withString:@""];
    //
    //    float t = [TAX floatValue];
    //    float tot = [TOTAL floatValue];
    //
    //
    //    //NSLog(@"Tax is %@",TAX);
    //    //NSLog(@"Total is %@",TOTAL);
    //
    //    if (t > 0.0 && tot > 0.0) {
    //
    //        float salesTaxPercent = (t/(tot-t))*100;   // previous (t/(tot))*100;
    //
    //        _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"%.2f",salesTaxPercent];
    //    }else
    //        _salesTaxPercentageTextField.text = [NSString stringWithFormat:@"0.00"];
    //
    return YES;
}

-(void) updateUserDefaults{
    
    float defaultsValue = [[NSUserDefaults standardUserDefaults] floatForKey:@"totalAmount"];
    float oldValue = [editTotal floatValue];
    
    float newValue = [_totalText.text floatValue];
    
    //NSLog(@"oldvalue %.2f",oldValue);
    //NSLog(@"newvalue %.2f",oldValue);
    
    float resultantValue;
    
    if(newValue > oldValue)
    {
        resultantValue = newValue-oldValue+defaultsValue;
    }
    else if(newValue == oldValue)
    {
        resultantValue = defaultsValue;
    }
    else
    {
        resultantValue = defaultsValue-(oldValue-newValue);
    }
    
    [[NSUserDefaults standardUserDefaults] setFloat:resultantValue forKey:@"totalAmount"];
    
    
    float defaultsValueTax = [[NSUserDefaults standardUserDefaults] floatForKey:@"totalSalesTax"];
    
    
    float oldValueTax = [editTax floatValue];
    
    NSString *NEWVALUETAX = [_taxText.text
                             stringByReplacingOccurrencesOfString:@"$" withString:@""];
    
    float newValueTax = [NEWVALUETAX floatValue];
    
    //NSLog(@"OLDValueTax %.2f",oldValueTax);
    //NSLog(@"NewValueTax %.2f",newValueTax);
    
    float resultantValueTax;
    
    if(newValueTax > oldValueTax)
    {
        resultantValueTax =newValueTax-oldValueTax+defaultsValueTax;
    }
    else if(newValueTax == oldValueTax)
    {
        resultantValueTax = defaultsValueTax;
    }
    else
    {
        resultantValueTax = defaultsValueTax-(oldValueTax-newValueTax) ;
    }
    
    //NSLog(@"resultantValueTax %.2f",resultantValueTax);
    
    [[NSUserDefaults standardUserDefaults] setObject:[[NSNumber alloc]initWithBool:YES] forKey:ShouldFetchRecordsKey];
    
    [[NSUserDefaults standardUserDefaults] setFloat:resultantValueTax forKey:@"totalSalesTax"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Receipt Updated"] message:@"Receipt Has Been Updated Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
    alertView.tag = 2;
    alertView.alertViewStyle = UIAlertViewStyleDefault;
    [alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 2 && buttonIndex == 0)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}
- (IBAction)backTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}




- (IBAction)doneTapped:(id)sender
{
   
        
        isPickerViewVisible = NO;
        [_pickerTopView setHidden:YES];
    [_dateTextField resignFirstResponder];
    [self.Category resignFirstResponder];
    [myPickerView resignFirstResponder];


}

- (NSData *)base64DataFromString: (NSString *)string
{
    unsigned long ixtext, lentext;
    unsigned char ch, inbuf[4], outbuf[3];
    short i, ixinbuf;
    Boolean flignore, flendtext = false;
    const unsigned char *tempcstring;
    NSMutableData *theData;
    
    if (string == nil)
    {
        return [NSData data];
    }
    ixtext = 0;
    tempcstring = (const unsigned char *)[string UTF8String];
    lentext = [string length];
    theData = [NSMutableData dataWithCapacity: lentext];
    
    ixinbuf = 0;
    
    while (true)
    {
        if (ixtext >= lentext)
        {
            break;
        }
        
        ch = tempcstring [ixtext++];
        
        flignore = false;
        
        if ((ch >= 'A') && (ch <= 'Z'))
        {
            ch = ch - 'A';
        }
        else if ((ch >= 'a') && (ch <= 'z'))
        {
            ch = ch - 'a' + 26;
        }
        else if ((ch >= '0') && (ch <= '9'))
        {
            ch = ch - '0' + 52;
        }
        else if (ch == '+')
        {
            ch = 62;
        }
        else if (ch == '=')
        {
            flendtext = true;
        }
        else if (ch == '/')
        {
            ch = 63;
        }
        else
        {
            flignore = true;
        }
        
        if (!flignore)
        {
            short ctcharsinbuf = 3;
            Boolean flbreak = false;
            
            if (flendtext)
            {
                if (ixinbuf == 0)
                {
                    break;
                }
                
                if ((ixinbuf == 1) || (ixinbuf == 2))
                {
                    ctcharsinbuf = 1;
                }
                else
                {
                    ctcharsinbuf = 2;
                }
                
                ixinbuf = 3;
                
                flbreak = true;
            }
            
            inbuf [ixinbuf++] = ch;
            
            if (ixinbuf == 4)
            {
                ixinbuf = 0;
                
                outbuf[0] = (inbuf[0] << 2) | ((inbuf[1] & 0x30) >> 4);
                outbuf[1] = ((inbuf[1] & 0x0F) << 4) | ((inbuf[2] & 0x3C) >> 2);
                outbuf[2] = ((inbuf[2] & 0x03) << 6) | (inbuf[3] & 0x3F);
                
                for (i = 0; i < ctcharsinbuf; i++)
                {
                    [theData appendBytes: &outbuf[i] length: 1];
                }
            }
            
            if (flbreak)
            {
                break;
            }
        }
    }
    
    return theData;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_taxText resignFirstResponder];
    [_dateTextField resignFirstResponder];
    [_Category resignFirstResponder];
    [_totalText resignFirstResponder];
    [_salesTaxPercentageTextField resignFirstResponder];
    [_vendorNameText resignFirstResponder];
    [_Description resignFirstResponder];
}


@end
