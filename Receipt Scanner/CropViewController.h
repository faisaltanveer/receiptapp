//
//  CropViewController.h
//  Receipt Scanner
//
//  Created by Inam ur Rahman on 1/22/14.
//  Copyright (c) 2014 Horizon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TesseractOCR/Tesseract.h>
#import "BJImageCropper.h"
#import "GPUImage.h"
#import "ImageProcessing.h"
#import "PresentResultsViewController.h"

#import "MBProgressHUD.h"
#import "ActivityIndicatorBox.h"

@interface CropViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property UIImage *imageToCrop;
@property UIImageView *imageView;
@property BJImageCropper *imageCropper;
@property BJImageCropper *imageCropperForDetails;
@property (strong ,nonatomic) UIImage *binarizedImage;
@property (strong, nonatomic) ImageProcessing *imageProcessor;
@property (strong, nonatomic) IBOutlet UILabel *cropStepsLabel;

@property (strong, nonatomic) IBOutlet UIButton *scanButton;

- (IBAction)backTapped:(id)sender;
- (IBAction)scanVendorNamePressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *cropImageView;
@property (strong, nonatomic) IBOutlet UIView *croppingView;

@property BOOL isManual;

@property (assign, nonatomic) int imageSizeInBytes;

@end
