//
//  RagistrationViewController.h
//  ASKever
//
//  Created by Harshal Bajaj on 3/31/14.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <QuartzCore/QuartzCore.h>

extern NSString *nameText , *askIdText, *cellNoText , *emailText, *passwordText;

@interface RagistrationViewController : UIViewController  <SelectCountryDelegate,UIAlertViewDelegate, UITextFieldDelegate>
{
    IBOutlet UIScrollView *scrollview;
    IBOutlet UITextField *nameTxt;
    IBOutlet UITextField *cellNoTxt;
    IBOutlet UITextField *emailTxt;
    IBOutlet UITextField *passwordTxt;
    IBOutlet UITextField *retypepswTxt;
    
    IBOutlet UIButton *btnCountryCode;
    
    NSString *countryWithCode;
   
	BOOL keyboardVisible;
	CGPoint offset;
	UITextField *activeField;
    
}
- (IBAction)signUpPushed:(id)sender;

@property(nonatomic,retain) IBOutlet UIScrollView *scrollview;
@property(nonatomic,retain) IBOutlet UITextField *nameTxt;
@property (nonatomic , retain) IBOutlet UITextField *cellNoTxt;
@property (nonatomic , retain) IBOutlet UITextField *emailTxt;
@property (nonatomic , retain) IBOutlet UITextField *passwordTxt;
@property (nonatomic , retain) IBOutlet UITextField *retypepswTxt;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property(nonatomic,retain) IBOutlet UIButton *btn_signUp;
@property (strong, nonatomic) IBOutlet UIButton *AlreadyRegUser;

//-(IBAction)back;
-(IBAction)selectCountryCode:(id)sender;

-(void)selectCountry:(NSString *)countryName countryCode:(NSString *)ccode;

//-(IBAction)alreadyRegisteredClicked:(id)sender;

@end
